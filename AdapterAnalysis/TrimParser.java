package AdapterAnalysis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;

public class TrimParser {

	
	public TrimParser(String filename) throws Exception
	{
		Scanner s = new Scanner(new File(filename));
		long total = 0;
		long trimmed = 0;
		long below_36 = 0;
		long[] lengths = null; 
		int badFormat = 0;
		while(s.hasNextLine())
		{
			try{					
				String[] split = s.nextLine().split(" ");
				int length = Integer.parseInt(split[split.length-4]);
				int original_length = Integer.parseInt(split[split.length-2])+Integer.parseInt(split[split.length-1]);
				//int l = original_length-length;
				if(lengths == null)
					lengths = new long[original_length+1];
				if(lengths.length < original_length+1)
				{
					long[] temp = new long[original_length+1];
					for (int i = 0; i < lengths.length; i++)
					{
						temp[i]=lengths[i];
					}
					lengths = temp;
					if(lengths.length-1 > 36)
						trimmed+=lengths[lengths.length-1];
				}
				lengths[length]++;
				if(length < original_length || length < 36)
				{
					trimmed++;
					if(length < 36)
						below_36++;
				}
				total++;
			}catch (Exception e){badFormat++;total++;}
		}
		System.out.println(String.format("* %3.3f%% reads trimmed\n* %3.3f%% reads too short\n* %3.3f%% bad format reads (thrown out due to trimming error)",((100.0*trimmed)/total),((100.0*below_36)/total),((100.0*badFormat)/total)));
		System.out.println();
		System.out.println("### Read length distributions ###");
		System.out.println();
		double sumPer = 0;
		for(int i =lengths.length -1; i >= 0; i--)
		{	
			if(i > lengths.length-10)
				System.out.println(String.format("\t%d\t%3.3f",i,((100.0*lengths[i])/total)));
			else
				sumPer+=100.0*lengths[i]/total;
		}
		System.out.println(String.format("\t<%d\t%3.3f",lengths.length-10,sumPer));
		s.close();
	}
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		try {
			new TrimParser(args[0]);
		} catch (Exception e) {
			//System.err.println("Usage: TrimParser TrimmomaticLogFile");
			e.printStackTrace();
		}
	}

}
