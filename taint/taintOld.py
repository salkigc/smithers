#!/usr/bin/env python

import os
from annotation import summarize_taxonomy
from assembly import quantify_assemblyOld

import argparse

parser = argparse.ArgumentParser()
parser.add_argument("shortname", help="prefix for resulting files")
parser.add_argument("output", help="directory where results will be saved")
parser.add_argument("reads", help="FASTQ file containing unmapped sequencing reads")
parser.add_argument("-2", "--read2", help="second file containing paired reads", default=None)
parser.add_argument("-w", "--working", help="a location to store intermediate files; if unspecified, these files "
                                            "will be generated in a temporary directory and automatically deleted",
                    default=None)
parser.add_argument("-k", "--kmer", help="minimum k-mer count for Trinity (default 2)", type=int, default=2)
parser.add_argument("-l", "--length", help="minimum contig length in Trinity assembly (default 200)", type=int,
                    default=200)
parser.add_argument("-s", "--skip", help="skip check for Sailfish library", action="store_true")
args = parser.parse_args()

# check for environment variable that Sailfish probably needs
if not os.environ.has_key("LD_LIBRARY_PATH") and not args.skip:
    parser.error("Sailfish libraries not available (set LD_LIBRARY_PATH)")

quantify_assemblyOld(args.reads, args.output, args.shortname, args.read2, working_directory=args.working,
                  kmer_cov=args.kmer, min_contig_length=args.length)

sailfish_file = os.path.join(args.output, "%s.sailfish.tsv" % args.shortname)
transcriptome_file = os.path.join(args.output, "%s.Trinity.fasta" % args.shortname)
output_csv_file = os.path.join(args.output, "%s.csv" % args.shortname)

summarize_taxonomy(sailfish_file, transcriptome_file, 0.0, output_csv_file=output_csv_file)
