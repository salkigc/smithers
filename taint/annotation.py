#!/usr/bin/env python

from StringIO import StringIO
from Bio import Entrez, SeqIO
import pandas
import subprocess

Entrez.email = "mchang@salk.edu"

SAILFISH_COLUMN_NAMES = ["Transcript", "Length", "TPM", "RPKM", "KPKM", "EstimatedNumKmers", "EstimatedNumReads"]
SAILFISH_HEADER_LINES = 5

ANNOTATION_COLUMNS = ["NCBI_GI", "NCBI_Title", "NCBI_Taxon"]
TAXONOMY_COLUMNS = ["NCBI_Taxon", "Organism"]
BLAST_NUM_RESULTS = 25
BLAST_THREADS = 8
BLAST_HEADER = ["Transcript", "NCBI_ID", "Identity", "NCBI_Taxon", "NCBI_Title", "Evalue", "BitScore"]
BLAST_COMMAND = "blastn -db nt -max_target_seqs %d -num_threads %d -outfmt '6 qseqid sseqid pident staxids stitle " \
                "evalue bitscore'" % (BLAST_NUM_RESULTS, BLAST_THREADS)


class AbundanceReporter:
    def __init__(self, sailfish_output, transcriptome_file):
        """
        :param sailfish_output: TSV output from Sailfish
        :param transcriptome_file: FASTA transcriptome assembly (e.g. from Trinity)
        """

        # parse Sailfish output and determine proportional counts
        self.abundance_table = pandas.read_table(sailfish_output, skiprows=SAILFISH_HEADER_LINES, header=None,
                                                 names=SAILFISH_COLUMN_NAMES, index_col=0)
        total_effective_count = sum(self.abundance_table['TPM'])
        self.abundance_table['proportional_count'] = self.abundance_table['TPM'] / total_effective_count
        self.abundance_table.sort('proportional_count', ascending=False, inplace=True)

        self.sequence_dict = {}
        for sequence in SeqIO.parse(transcriptome_file, "fasta"):
            self.sequence_dict[sequence.id] = sequence

    def get_abundant_transcripts(self, min_frequency=0.01):
        """
        return a list of transcript IDs that exceed a frequency threshold
        """
        relevant_entries = self.abundance_table['proportional_count'] >= min_frequency
        return self.abundance_table[relevant_entries].index.tolist()

    def identify_transcripts(self, transcript_list=None):
        """
        Use NCBI resources to identify the transcript sequences and their associated species.
        :param transcript_list: (optional) identify only transcripts present in this list
        """
        if transcript_list:
            sequence_list = []
            for transcript_id in transcript_list:
                sequence_list.append(self.sequence_dict[transcript_id])
        else:
            sequence_list = self.sequence_dict.values()

        fasta_handle = StringIO()
        SeqIO.write(sequence_list, fasta_handle, "fasta")
        fasta = fasta_handle.getvalue()

        blast_process = subprocess.Popen(BLAST_COMMAND, stdin=subprocess.PIPE, stdout=subprocess.PIPE, shell=True)
        blast_result, blast_errors = blast_process.communicate(fasta)
        blast_result_stringio = StringIO(blast_result)
        blast_table = pandas.read_table(blast_result_stringio, header=None, names=BLAST_HEADER)
        
        # NCBI_Taxon column sometimes contains semicolon-separated values, so leave out non-numeric rows
        blast_table['NCBI_Taxon'] = blast_table['NCBI_Taxon'].convert_objects(convert_numeric=True)
        blast_table = blast_table[blast_table['NCBI_Taxon'].notnull()]

        # Add a column containing maximum score for each sequence
        blast_table['score_max'] = blast_table.groupby('Transcript')['BitScore'].transform(max)
        # Keep only rows that contain the highest score in its group
        blast_table = blast_table[blast_table['BitScore'] == blast_table['score_max']]

        # generate a new data frame that contains only one entry per sequence (the first one)
        sequence_group = blast_table.groupby('Transcript')
        best_blast_table = sequence_group.first()

        # store a negative count of taxa present as Taxonomy ID when more than 1 taxon is present
        #ambiguous_taxon = sequence_group['NCBI_Taxon'].nunique() * -1
        #best_blast_table.loc[ambiguous_taxon < -1, 'NCBI_Taxon'] = ambiguous_taxon[ambiguous_taxon < -1]
        del(best_blast_table['score_max'])

        # join annotation with abundance, preserving all rows/keys in abundance table (left join)
        self.abundance_table = self.abundance_table.join(best_blast_table, how="left")

        # generate a list of unique GI
        NCBI_ID_series = self.abundance_table['NCBI_ID'].str.split('|').str[1]
        gi_list = NCBI_ID_series.dropna().unique()
        return gi_list

    def identify_taxa(self):
        # Look up non-negative NCBI taxon IDs
        ncbi_taxon_series = self.abundance_table['NCBI_Taxon'].dropna().astype(int)
        ncbi_taxon_series = ncbi_taxon_series[ncbi_taxon_series > 0]
        taxonomy_ids = ','.join(ncbi_taxon_series.astype(str).unique())

        session = Entrez.read(Entrez.epost("taxonomy", id=taxonomy_ids))
        webenv = session['WebEnv']
        query_key = session["QueryKey"]

        # get information about the relevant taxa
        taxon_list = Entrez.read(Entrez.efetch(db="taxonomy", webenv=webenv, query_key=query_key))
        name_list = []
        for taxon in taxon_list:
            simple_name = taxon['ScientificName']

            if taxon['Division'] == "Viruses":
                for lineage in taxon['LineageEx']:
                    if lineage['Rank'] == 'species':
                        simple_name = lineage['ScientificName']
                        break
            else:
                for lineage in taxon['LineageEx']:
                    if lineage['Rank'] == 'genus':
                        genus = lineage['ScientificName']
                        if genus == 'Homo':
                            simple_name = "Human"
                        elif genus == 'Mus':
                            simple_name = "Mouse"
                        else:
                            simple_name = genus
                        break

            name_list.append([int(taxon['TaxId']), simple_name])

        taxonomy_table = pandas.DataFrame.from_records(name_list, columns=TAXONOMY_COLUMNS, index="NCBI_Taxon")
        self.abundance_table = self.abundance_table.join(taxonomy_table, how="left", on="NCBI_Taxon")

        # Set "Ambiguous" organism in entries that have negative taxon IDs
        self.abundance_table.loc[self.abundance_table.NCBI_Taxon < 0, "Organism"] = "Ambiguous"

        # Set "Unidentified" organism when no BLAST hits were found
        self.abundance_table.loc[self.abundance_table.NCBI_ID.isnull(), "Organism"] = "Unidentified"

        return name_list

    def output_taxonomy_summary(self):
        organism_grouping = self.abundance_table.groupby("Organism")
        output_table = organism_grouping["EstimatedNumReads"].agg({"Transcripts": len, "Assembled reads": sum})
        output_table.sort("Assembled reads", ascending=False, inplace=True)
        read_total = output_table["Assembled reads"].sum()
        output_table['Proportion'] = output_table['Assembled reads']/read_total

        print output_table.to_string( columns=['Assembled reads', 'Proportion', 'Transcripts'],
                                      formatters={"Assembled reads": '{:,.0f}'.format, "Proportion": '{:,.1%}'.format})

        # print "Transcriptome assembly generated %d sequences to which %d reads were mapped." % \
        #       (len(self.abundance_table), total)
        #
        # print "\nOrganism: Number of reads (% of mapped reads)"
        # for organism, value in reads_series.iteritems():
        #     print "%s: %d (%.1f%%)" % (organism, value, value/total*100.0)

        # (formatters={"Assembled reads": f, "Proportion": '{:,.1%}'.format})
        # '{:,.0f}'.format # float with thousands separator, rounded off
        # '{:,.1%}'.format} # percentage with 1 decimal


def summarize_taxonomy(sailfish_file, transcriptome_file, threshold, input_csv_file=None, output_csv_file=None):
    """

    :param sailfish_file:
    :param transcriptome_file:
    :param threshold:
    :param input_csv_file:
    :param output_csv_file:
    :return:
    """
    reporter = AbundanceReporter(sailfish_file, transcriptome_file)
    if input_csv_file:
        reporter.abundance_table = pandas.read_csv(input_csv_file, index_col=0)
    else:
        relevant_transcripts = reporter.get_abundant_transcripts(threshold)
        reporter.identify_transcripts(relevant_transcripts)
        reporter.identify_taxa()

    if output_csv_file:
        reporter.abundance_table.to_csv(output_csv_file)

    reporter.output_taxonomy_summary()

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("sailfish", help="TSV result from Sailfish")
    parser.add_argument("transcriptome", help="Transcriptome assembly")
    parser.add_argument("-t", "--threshold", help="Minimum proportion of total effective counts (default = 0.0)",
                        type=float, default=0.0)
    parser.add_argument("-c", "--csv", help="CSV file with existing sequence annotation", default=None)
    parser.add_argument("-f", "--file", help="Write annotated results to a CSV file", default=None)

    args = parser.parse_args()
    summarize_taxonomy(args.sailfish, args.transcriptome, args.threshold, args.csv, args.file)

