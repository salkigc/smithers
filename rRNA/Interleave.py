#!/usr/bin/env python

# see http://news.open-bio.org/news/2009/12/interleaving-paired-fastq-files-with-biopython/

import gzip
import itertools
from Bio.SeqIO.QualityIO import FastqGeneralIterator


def interleave_fastq_gz(read1_filename, read2_filename, output_filename):
    """

    :param read1_filename:
    :param read2_filename:
    :param output_filename:
    :return:
    """

    if read1_filename.endswith(".gz"):
        opener = gzip.open
    else:
        opener = open

    with opener(read1_filename, 'rb') as read1, opener(read2_filename, 'rb') as read2, open(output_filename, 'w') as output:
        read1_iterator = FastqGeneralIterator(read1)
        read2_iterator = FastqGeneralIterator(read2)

        for (read1_id, read1_seq, read1_q), (read2_id, read2_seq, read2_q) in \
                itertools.izip(read1_iterator, read2_iterator):
            assert read1_id.split(' ')[0] == read2_id.split(' ')[0], "Reads are not matched."
            output.write("@%s\n%s\n+\n%s\n@%s\n%s\n+\n%s\n" %
                         (read1_id, read1_seq, read1_q, read2_id, read2_seq, read2_q))


def split_interleaved_fastq(input_filename, read1_filename, read2_filename):
    """

    :param input_filename:
    :param read1_filename:
    :param read2_filename:
    :return:
    """
    if read1_filename.endswith(".gz"):
        writer = gzip.open
    else:
        writer = open

    with writer(read1_filename, 'w') as read1, \
            writer(read2_filename, 'w') as read2, \
            open(input_filename, 'r') as input_handle:

        for (read_id, read_seq, read_q) in FastqGeneralIterator(input_handle):
            (read_id1, read_id2) = read_id.split(' ')
            read_end = read_id2.split(':')[0]
            # first read
            if read_end == '1':
                read1.write("@%s\n%s\n+\n%s\n" % (read_id, read_seq, read_q))
                first_id = read_id1

            # second read
            else:
                assert first_id == read_id1, "File does not appear interleaved."
                read2.write("@%s\n%s\n+\n%s\n" % (read_id, read_seq, read_q))


if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("read1", help="R1 read")
    parser.add_argument("read2", help="R2 read")
    parser.add_argument("merged", help="merged FASTQ filename")
    parser.add_argument("-s", "--split", help="Split FASTQ rather than interleave", action="store_true")

    args = parser.parse_args()

    if args.split:
        split_interleaved_fastq(args.merged, args.read1, args.read2)
    else:
        interleave_fastq_gz(args.read1, args.read2, args.merged)
