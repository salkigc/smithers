#!/usr/bin/env python

"""

"""

import os
import gzip
import subprocess
import tempfile
import shutil
from string import Template
import shlex
import pandas
from Interleave import interleave_fastq_gz, split_interleaved_fastq
from contextlib import contextmanager


DEFAULT_RRNA_DATABASE_LOCATION = "/genomics/mchang/rRNA"
SORTMERNA_COMMAND = Template("sortmerna --ref "
                             "$rrna_db/fasta/silva-bac-16s-id90.fasta,$rrna_db/index/silva-bac-16s-db:"
                             "$rrna_db/fasta/silva-bac-23s-id98.fasta,$rrna_db/index/silva-bac-23s-db:"
                             "$rrna_db/fasta/silva-arc-16s-id95.fasta,$rrna_db/index/silva-arc-16s-db:"
                             "$rrna_db/fasta/silva-arc-23s-id98.fasta,$rrna_db/index/silva-arc-23s-db:"
                             "$rrna_db/fasta/silva-euk-18s-id95.fasta,$rrna_db/index/silva-euk-18s-db:"
                             "$rrna_db/fasta/silva-euk-28s-id98.fasta,$rrna_db/index/silva-euk-28s:"
                             "$rrna_db/fasta/rfam-5s-database-id98.fasta,$rrna_db/index/rfam-5s-db:"
                             "$rrna_db/fasta/rfam-5.8s-database-id98.fasta,$rrna_db/index/rfam-5.8s-db "
                             "--reads $input_filename "
                             "--aligned $rrna_basename "
                             "--other $output_basename "
                             "-a $cpu "
                             "$paired "
                             "--blast 1 --log --fastx --best 1")

SORTMERNA_OPTIONS = {"rrna_db": DEFAULT_RRNA_DATABASE_LOCATION,
                     "input_filename": None,
                     "rrna_basename": "rRNA",
                     "output_basename": "output",
                     "paired": "",
                     "cpu": 24}
PAIRED_INPUT_FLAG = "--paired_in"
FILTERED_FILENAME_SUFFIX = '_filtered'


@contextmanager
def TemporaryDirectory():
    """
    see http://stackoverflow.com/questions/6884991/how-to-delete-dir-created-by-python-tempfile-mkdtemp
    :return:
    """
    name = tempfile.mkdtemp()
    try:
        yield name
    finally:
        shutil.rmtree(name)


def parse_log(log_filename, quiet=False):
    """

    :param log_filename:
    :return:

Results:
    Total reads = 1000000
    Total reads passing E-value threshold = 227180 (22.72%)
    Total reads failing E-value threshold = 772820 (77.28%)
    Minimum read length = 51
    Maximum read length = 51
    Mean read length = 51
 By database:
    /genomics/mchang/rRNA/fasta/silva-bac-16s-id90.fasta		0.02%
    /genomics/mchang/rRNA/fasta/silva-bac-23s-id98.fasta		0.04%
    /genomics/mchang/rRNA/fasta/silva-arc-16s-id95.fasta		0.00%
    /genomics/mchang/rRNA/fasta/silva-arc-23s-id98.fasta		0.00%
    /genomics/mchang/rRNA/fasta/silva-euk-18s-id95.fasta		6.14%
    /genomics/mchang/rRNA/fasta/silva-euk-28s-id98.fasta		14.89%
    /genomics/mchang/rRNA/fasta/rfam-5s-database-id98.fasta		1.00%
    /genomics/mchang/rRNA/fasta/rfam-5.8s-database-id98.fasta		0.62%
    """

    rRNA_source = ["bacterial 16s", "bacterial 23s", "archea 16s", "archea 23s", "eukaryotic 18s", "eukaryotic 28s",
                   "5s", "5.8s"]
    rRNA_fraction = []

    with open(log_filename, 'r') as logfile:
        contents = logfile.readlines()
        result_index = contents.index(' Results:\n')
        total_reads = int(contents[result_index+1].split()[-1])
        rRNA_reads = int(contents[result_index+2].split()[-2])

        if not quiet:
            for i in range(8, 16):
                rRNA_fraction.append(float(contents[result_index+i].split()[-1][0:-1])/100)

            rRNA_source_table = pandas.DataFrame.from_items([('rRNA type', rRNA_source), ('Fraction', rRNA_fraction)])
            print "From a total of {:,} reads, {:,} were classified as rRNA ({:.1%})\n".\
                format(total_reads, rRNA_reads, float(rRNA_reads)/total_reads)

            print rRNA_source_table.to_string(index=False, float_format='{:,.2%}'.format)

    return rRNA_reads, float(rRNA_reads)/total_reads


def filter_fastq(read1_filename, read2_filename=None, quiet=False):
    """
    1. Unzip, interleave, or copy FASTQ file(s) to a temporary directory for use with SortMeRNA.
    2. Run SortMeRNA with defined output
    3. Parse log
    4. Split/copy filtered FASTQ
    5. Clean up temporary directory

    :param read1_filename:
    :param read2_filename:
    :return:
    """
    with TemporaryDirectory() as working_directory:
        input_filename = os.path.join(working_directory, "input.fastq")
        output_directory = os.path.abspath(os.path.dirname(read1_filename))
        current_sortmerna_options = SORTMERNA_OPTIONS.copy()

        # interleaving takes care of gzipped or non-gzipped input files
        if read2_filename:
            interleave_fastq_gz(read1_filename, read2_filename, input_filename)
            current_sortmerna_options['paired'] = PAIRED_INPUT_FLAG

        # for only a single FASTQ
        else:
            # gzipped input file
            if read1_filename.endswith('.gz'):
                with gzip.open(read1_filename, 'rb') as gzipped_input, open(input_filename, 'w') as fastq_output:
                    for line in gzipped_input:
                        fastq_output.write(line)
            else:
                input_filename = os.path.abspath(read1_filename)

            # write result directly to current directory
            output_basename = os.path.join(output_directory,
                                           os.path.basename(read1_filename).split('.')[0] + FILTERED_FILENAME_SUFFIX)
            current_sortmerna_options['output_basename'] = output_basename

        # set up and execute SortMeRNA
        current_sortmerna_options['input_filename'] = input_filename
        current_sortmerna_command = SORTMERNA_COMMAND.substitute(current_sortmerna_options)
        command_args = shlex.split(current_sortmerna_command)
        subprocess.check_call(command_args, cwd=working_directory, env=os.environ.copy())

        # split joined FASTQ to current directory
        if read2_filename:
            read1_result = os.path.join(output_directory, os.path.basename(read1_filename).split('.')[0]
                                        + FILTERED_FILENAME_SUFFIX + ".fastq")
            read2_result = os.path.join(output_directory, os.path.basename(read2_filename).split('.')[0]
                                        + FILTERED_FILENAME_SUFFIX + ".fastq")
            split_interleaved_fastq(os.path.join(working_directory,
                                                 current_sortmerna_options['output_basename'] + ".fastq"),
                                    read1_result, read2_result)

        log_filename = os.path.join(working_directory, current_sortmerna_options['rrna_basename'] + ".log")
        parse_log(log_filename, quiet=quiet)


if __name__ == '__main__':
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("read1", help="First read")
    parser.add_argument("read2", help="Second read", nargs="?", default=None)
    parser.add_argument("-q", "--quiet", help="Suppress output of classification results.", action="store_true")
    args = parser.parse_args()

    filter_fastq(args.read1, args.read2, quiet=args.quiet)
