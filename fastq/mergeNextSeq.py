#!/usr/bin/env python

"""
Demultiplexing Illumina NextSeq runs produces multiple gzipped fastq files, which is annoying.

This script will concatenate samples across lanes and split files, producing a single fastq file per read end.

For example, given a group of samples with names:
hu-MDM513-RNA-H5N1HALoMOI2-18h-2-140805_S8_L001_R1_001.fastq.gz
hu-MDM513-RNA-H5N1HALoMOI2-18h-2-140805_S8_L001_R2_001.fastq.gz
hu-MDM513-RNA-H5N1HALoMOI2-18h-2-140805_S8_L002_R1_001.fastq.gz
hu-MDM513-RNA-H5N1HALoMOI2-18h-2-140805_S8_L002_R2_001.fastq.gz
hu-MDM513-RNA-H5N1HALoMOI2-18h-2-140805_S8_L003_R1_001.fastq.gz
hu-MDM513-RNA-H5N1HALoMOI2-18h-2-140805_S8_L003_R2_001.fastq.gz
hu-MDM513-RNA-H5N1HALoMOI2-18h-2-140805_S8_L004_R1_001.fastq.gz
hu-MDM513-RNA-H5N1HALoMOI2-18h-2-140805_S8_L004_R2_001.fastq.gz

this script will produce: hu-MDM513-RNA-H5N1HALoMOI2-18h-2-140805_S8_R1.fastq.gz and
hu-MDM513-RNA-H5N1HALoMOI2-18h-2-140805_S8_R2.fastq.gz
"""

import os
import glob
import shutil

R1_PATTERN = ("*_L*_R1_*gz", "R1")
R2_PATTERN = ("*_L*_R2_*gz", "R2")
EXPECTED_FASTQ_DIRECTORY = "Data/Intensities/BaseCalls"


def concatenate_files(output_filename, input_file_list):
    with open(output_filename, 'wb') as output_file:
        for input_file in input_file_list:
            shutil.copyfileobj(open(input_file), output_file)


def group_fastq_files(directory, pattern):
    sample_group = {}
    for fastq_filename in sorted(glob.glob(os.path.join(directory, pattern))):
        filename_groupings = fastq_filename.split('_')
        sample_basename = '_'.join(filename_groupings[0:-3])
        sample_group.setdefault(sample_basename, []).append(fastq_filename)

    return sample_group


def merge_fastq_files(directory, remove_original_files=True, suppress_output=False):
    for pattern, read_end in (R1_PATTERN, R2_PATTERN):
        sample_group = group_fastq_files(directory, pattern)
        for sample_name, sample_list in sample_group.items():
            output_filename = "%s_%s.fastq.gz" % (sample_name, read_end)
            if len(sample_list) == 1:
                print "%s was not merged with other files." % sample_list[0]
            else:
                concatenate_files(output_filename, sample_list)
                if not suppress_output:
                    print "%s constructed from %d files:" % (output_filename, len(sample_list))
                    for sample_filename in sample_list:
                        print "\t%s" % sample_filename

                if remove_original_files:
                    for sample_filename in sample_list:
                        os.remove(sample_filename)

if __name__ == '__main__':
    import argparse

    parser = argparse.ArgumentParser(description="Merge multi-lane fastq.gz files (as produced by Illumina NextSeq). By default, the original files will be deleted.")
    parser.add_argument("-d", "--directory", help="Directory containing fastq files", default=".")
    parser.add_argument("-k", "--keep", action="store_false", help="Do not delete original fastq files", default=True)
    parser.add_argument("-q", "--quiet", action="store_true", default=False)
    args = parser.parse_args()

    if os.path.exists(EXPECTED_FASTQ_DIRECTORY):
        merge_fastq_files(EXPECTED_FASTQ_DIRECTORY, args.keep, args.quiet)
    else:
        merge_fastq_files(args.directory, args.keep, args.quiet)
