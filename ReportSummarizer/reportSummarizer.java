package ReportSummarizer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Scanner;

/**
 * This program generates markdown output for the Smithers QC pipeline summarizing the individual file reports
 * 
 * 
 * @author maxsh
 *
 */
public class reportSummarizer {
	
	private static String check = "<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAZCAMAAAAYAM5SAAAAAXNSR0IArs4c6QAAAJxQTFRFAAAAAIQACIQIEIQQEIwQGIwYIZQhKZQpMZQxMZwxOZw5QqVCSqVKUqVSUq1SWq1aY7Vja61ra7Vrc7Vzc71ze717hMaEjL2MjMaMlL2UlMaUlM6UnM6cpc6lrdattd61vc69vda9vd69xs7GxtbGxt7Gzs7OzufO1tbW1t7W1ufW3t7e3ufe3u/e5+fn5+/n5/fn7+/v7/fv9/f32egFsgAAAAF0Uk5TAEDm2GYAAAABYktHRACIBR1IAAAACXBIWXMAAAsTAAALEwEAmpwYAAAAB3RJTUUH2QYFDTcs63y9UQAAAOVJREFUKM91kdsOgjAQRFmwlCDeIFwk4gVjLVSLjf//b25RCdI6D/Bw2JnZxXEsKiLHrntIEjs5eDRMrSQDSotOKWWQNRAfiZTqOSFL8EjWSSmkMgiQ9CaFMBASL25tJAFwlxcpuEFKAAj3NlIj8UvBNfm0Kwi54qubYVDSaPQla/0xsjkGLU790HejR4gs6XJ8BtXbbsjYE1ymctEubTjnPxV2OIAYFkc+CurVUtDyt5wbvfVC4K7OUzs95ukOW8Ymdloxhm2OjJn3duooyM/9kPlrn3g5Nlp2LGW76sDkH4KeRrkXMzkdgTp6200AAAAASUVORK5CYII=\" alt=\"fine\"";
	private static String bad2 = "<img src=\"data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAB4AAAAeCAYAAAA7MK6iAAAAB3RJTUUH2wkdFS8jsdiD+wAAAAlwSFlzAAALEgAACxIB0t1+/AAAAARnQU1BAACxjwv8YQUAAAkxSURBVHjalVd7cFTVGf/d1+5mN6/dDYl5mMSMWKjSiuFhRw2IpTDiaAe11SJI6RMrldo/nAGs7RRm6hRNmdZnx9rwqI+pcUptCSOIMghYE6Bqa4ZqIYGEhCSbfd179z6/fucmpKFFbLM5e+7jnO93zu/3fd/5VsL/8Nf3clt9z2v77pF6Ty1SdL3F7/kIWioHgg8/UQ6lsRE2tA6qrdt91Xfu6ShdfGv3p9mULgq47Tc3nHr+hbX28Q+XRgYHoYajiNXXoqiuDnJZDOQSvNEM7L7TMPr7UXBs2JXV0K6e2dFw26INNd+6r+v/AqbOzmjXlic3Zve98YPioX4krpyB2B1LEb/+OqjxBOB5INuGXLBAFveWBWegD/q7XUi/8SZyJ04iV1mFyMIFrc1r7t8gzZplfCpwaufO+vce27JNO3ioJVl3CaoeXIPSBYvh8Ue2HEiOBYjGgGRbDO7A515icNgOyMzD7DyGoV170Z8+C3nOtfsbVn/79tply4Yn46iTb4zXX6//y6af7o+//W5DouV6VGz6MdTiOBvOMYAP2RbGbW5WsGMIMF7M2HMLPlNNvBBtahPqy8sQ27UHfYffbTmhSJ1suyW6cGHvOSxlMr1vb3j4ldKDhz+fXLIIVRt/AlXRmEodksEgBR0wDd6RDjJEz+wZBX5mBs980RdMqHoBkm7AJQexS6qgGTm4R46V9/b0ND/Xc/K3/wV8s+k+qnTsvqtqzhzU/Ggdr0QOjMpsXDLyUPR8ACbnGYyBJd3k50bQZJ13bPDCGNAzmX5dH39no7isBFIuh9Hu7oY1d99b9vT7R3dPaNy9efO0wV88+WG1U0DdU63QyiqhFGz4HtNnM4DlwlVVSPlssHPVYC0dJ9BUaO07OThSlMOLEBo5EzidzOuDZbIf8vxMHgPHjuKfyRpUrf7G9Gnr13fLAji970BrnL2y6u47oUVLIefysApZUD4DKZuD7UtQrm0GZl0DP8+05zJQshnImTSUTAamI0G94xaElt8NIxyDn9Xhibk5XmiWmbILqEgmUXGmB4OH3mkVmLLx8sv1zrGji2O1tYjNncurS8HXRxFK5yGP5pGXVIRa5kKtaYA2fQbkli+wAkxphp00nYbpOwjfeQfkmbMhTbsKxV9fBSNSAsqOwmNgOZPlRWYRYumSCit75OhigxOSsiQW+17RoUM3JW/+IqKNTeOaMsVs3OZxkZtaIF/ayN7AN5IMZcoUWP0pjD79LHJn+xF+4AFoLfMgy/xe9uBXVyNUWwNr7x4ogwPMDjPHDBLLpLoeMqz3qUjxsEqDA0sjbLT8ium8Sh2Ox+HB3imbWVjvfQg/EkZ4WR2kUJiBFVA4gujXbofnstblCUTnLWQ/ZP0lVthTIDEDzqkTsI4fhzaSYu+VIBPxNzdJQjlfDfQOLFKVodHmaGkJ1EgkcB5fZKR0Ctldu6F+dBwmZyLfZ+9cvhKkRQJ3lLmPrvpmEBKuLLFBFxIpQUZzX9kO8/vrUZ5K8TMBKAU+TLww/kdUlhHOZVtUqfcEwolkEHu+40Jlx0l1/BmlH/dA4YlaLo3sQ+ugeEWIrLiLQTm2VQUayWPxzxlNcSXG5J22/x75tRtQnDnLc+XzEqMvHIpbhGeg9x+Q1ZFRaEW827QORez0zb0o+/gkVOIUycM1HleazsJ86IfIvrSVEwMJtIl8K5MEn+/NP74Efe39iA+fheYhCK0L5WbBkppKj6dM3qmv8ynT/XeEu4+Pr48NCt34owisEIdMUZyfekGmlcbNCd18ckEpm2NfgsuDFTGdJh8DNAEu6HbFV2dyCvVfPpWsW28hIxIhh73E49c29xyeZMlsMzGF9O1t5BkWmS7D+B7Z3FzXI09cuw45Ro7ST/2KRkrKyWAfZL7IDxqC5vK1IGJElqircgrh8OzZ1FORoHxDPYPJJNxETHL5WgAPJytJf6GNnIJBlsegrkuexWC2Q65lkeN55AeL4WbmGXwLpWKlwQbOgQngMZugPlmmg3PnkOwnKrsK7FDa6f7AmeRxPxQU6+VVKPrlYwh/+U74Woj15oqDPYQhYbz6IjLbd0DilOrJPI/Z80IhlKxYBWXzRuSKS1mq8055FlACZ3YOw2SXiqpke9aTmuFJgbLKpIFUpEGpSMDhU0oTocJZTOKjz2tvh7V2DRTWwXRthFauYNnDwivgcaiFmhpRiMYADk8htVBYD7Mtx4MuPKiivB2ptrb6N+vq6SxraUnShDait1mPwcbLqbC3g88Ek4uNApm/e44GK6oCHxBaDpUlKfvsU6yxQaZjk733NRpsaCRTUQL/0LnPVlaRs+AG6ototK+2lgRmsLe9183f9r4ikyGPaSyARS8mCvCR+iYq7P4Tmdu30nCikgyVn487nqlKNBCPU+7XT5C5eyeNNFxGeXGM8zxTVih3+RXkLPsq5VpuoL+qIXp7/sJXJkjt37KleX9NQyC8JSHwauEIrvBGacz4cG0NjSTjwU69gJFxJxxn5mx5GY1UVzOYGozJaxrps5rJvv8+Kqz+LvWUxulAbRP1b3my+bwAP3j7VzYeVMOUYmrESr3AE8eAg7BiY5Ol8CdJ4ojwE4sW1DIb6ViM9NuWkLNuPXmPPEynPjOVDoXC1Ll85eMXrCwPXN/y1vsMmmEjgkpXwqRw+Pf1fwK7482QNUo3NfEuV5O/6VGyWjfT0DVz6WhIoz3z5r31SaVuUOwdmLfg5DFhQJECCr0JUGmCgfPuMTYuU1xC2SWLyfnZo+Q88QyZrT+n0zM+R0dZ1/3z553s27Gj4qLlrRjQ07a1A2/sb27i0qdE9oOEr/p+EOEiPEQqlfk0KvDJpBdz+MycieL5N0KqrucTTEL+cCeGXv0Dhrio8Li8veqR9csTkyrMCwKfo/2D59ueGdq5855L+k+jitGKxItwCF4swq0UqOS6bGojop+9Gn5ZGZz0CIwP/gbjwCEMD51BtrIG0Rvnt87Z1vbghTAu+hOme9PmacPvHGj1uo4sjp45gzLeaREnAq0sAb+0mA8Ilc/vAmxRd3FBxwcrjMoKeNOvbG9cMH/9ZVzUfZLtiwKf+yt07Jx25MWXliqDqaXgwkHmH23yaCqg266Iw2u8FHI82eVXTmmfdtOXtifuvbf302z+C+miZvp8wB86AAAAAElFTkSuQmCC\" alt=\"suspicious\"";
	private static String bad = "<img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBwgHBgkIBwgKCgkLDRYPDQwMDRsUFRAWIB0iIiAdHx8kKDQsJCYxJyofLT0tMTU3Ojo6Iys/RD84QzQ5MCwBCgoKDQwNGhAPGzQkHyQ0NC42MDY3NDQ3Nzc2LDQ0NzI0NzQ0LDQ0LC8sNC8sNDQ0NDQ0NywsNDQsLCw0NDQsLP/AABEIACwALAMBEQACEQEDEQH/xAAaAAADAQEBAQAAAAAAAAAAAAAEBQYHAwIB/8QAMxAAAQMDAQQHBwUBAAAAAAAAAQIDBAAFESEGEjFBEyIyUXGRoRUjUmFigdEkcpPh8BT/xAAaAQABBQEAAAAAAAAAAAAAAAADAAECBAUG/8QALREAAgIBAgMHAgcAAAAAAAAAAQIAAxEEMQUhQRITIjJRYaHB0RQVI3GBkbH/2gAMAwEAAhEDEQA/ANxpRQK43SJbwBIWS4rsNIG8tXgKr36mqgeM/wAdZNK2faBG7XFWrNkfKORcdCT5a1W/G2nmtLfAk+6UbsJ99vBggXKDJhg6dIob6PMfin/MFU4tUr++39xdyT5TmN2nEOtpcaWlaFDIUk5Bq+rBhkQJGN56p4oBe7km1wFyCApZO62n4lHh+ftVbV6gaeouZOtO22ItszCIzYly1hyc+N5bqzqM8h3CsvTMF/Vs5uev0h7OfhGwjX/ozzzVg6yQ7ucnHgpJSoBQPEHXNAfVgjB5iSCRGJPsGclxon2e+rDrXJs/EO6q+n1Y0toUeRvgwjJ3i+4lYCCMg5BrpJSkht24TJgM5wnC1/fQVz3HnIRVlzSjczozYIc23pemtqcLoyCtZKvHPLwFQqrc0C1jv0js2G7IizZpbsAzrc44Voiv4Rvckn/etZepvKsPeGCgiP0KU72cYPAmg1M9x7IMRAEnLvPW/vssRlPNJJy9vAIJ+nPa8ae1CvhsbHpJL6iVmykkyrDFUokqQC2c8eqcD0xXY6Cw2adSZn3DDmKtvo6ughzUjqtLKF6cArGD5jH3qjxukvSGHSE0zYbEjrFd9o5e3TdtixXGrKAS+8E9UDdJBBOmc4GKJRVp9VUGQ9kncD7bfEZ2dGwecqr1b0Qnn32ZaXJEpG4poo1P1aHSsviXD6qFDl+fpD02l+WJMzdrX5W1EHZiPAexJWlMh5APURjXlw76noeFI6d4LM59pGy4qcYju+vNNudC07vhpG5pgBPyAGgxVbjAq7aVockbn6SdHawSespdkWVM2GMVDBcy5j5KOR6Yro+H1mvTKDKlxy5jKbHZlRXY8kAtOpKVA91W3QOpVtjBgkHIma3KG7ZJfQuOBxon3TyFdodxxwNchrdFdpXLITj1E0K7A4g6rpKZc/SRkKB7S1HU+ZrP7tbOdrnMJk9BCV3EugF1lCVY4g0JVZPIxj59Z2sltcvssA9WEhXvV8N76U/M+la/DeGtawd/L/sDdcFGBvNJSkJSEpAAAwAOVdeBiZ8BuUBqS2S4pwftVilFIS97PxFhW85I/k/qmIBGDEDiIRbEsdVuXK3e5SwrHmKpPw3TOclYYXOIZFs7UkgPSJKgeICwB6Cnr4bpqzkLGN7mWlhscVltG4p4AcBv6D0q6AAOUFKZCA2kJTnA7zSin//Z\" alt=\"suspicious please check individual sample reports for more info\"";
	private static String smithers = "<img src=\"data:image/gif;base64,R0lGODlh2gDkAOZ/AJWFasqpCDlCUomcvEJSUnSElJSqydjk9o6Ohv/nIRchBvfeEHmIpe/OFRMTBOnw+/nOIaS52C4xLElTZLSttZqbpM/a82lrc6/B3rrI5SYDAA5Jo2V0lcLR6vv9/xYgL3iKsbXF41RUXMrX7vD1/VdjcwMDADk5Qv/aBwchTANV1BYRF+jv+46jxgA0ogAKLxUDA52wzh8hGUtVfiEYCCEhAH2TstHd82xsZAgQAcvZ8CEpCPfWIVpaY/f//ykQCHN7c3t7c//WICEhCCkYADkpGCEpAFpCGN6cEIQ5CO+tGIRzECApQlZcb1JaOZR7CODp+e+9EKWUEKV7CHtjId61EEhPXOfe587Ozt69CGBsg3tzMdbW1u/v972ECFtsh+/n7+elEI6jvv/eIfjv8GNjWv//9ztCYcacCIRzMQAAEP+tGHtzY/f39yExAP+9EP/vHb3N6Ix7WnB2gyE5a6Cu03FxeSQcEGNwcAgQEA4ICxg5eyILE//OCAAIQv///yH5BAEAAH8ALAAAAADaAOQAAAf/gH+Cg4SFhoeIiYqLjI2Oj5CRkpOUlZaXmJmam5ydnp+goaKjpKWmp6ipqqunJBhFsKyys7SeGCJAQEEYtb2+v4kjA8McTkG5QBi8wMzNsiRNutLTQU7O19igJAfcDxHTucfHQEfZ5ueWGQxFR07t4eAXx0436Pb3hwdxHToZRePikB2TF/ADCXwI7d0wcAGIE2PwBOYiCI9HkTkJM2IbUUQaQInwxuniQZKkGCgaU9by4OHPl4giP8oLRzJKAh5ZqnhZ8qHDQZVAUXUQQ7RJTHgzPfKoskDITZIBnlDZMuQEg59Bs4aKg8DjtKTzxkUZM4ZH2ZI8jggAwBbAli9a/+N68iAhYkhdTqRVQcu35JG2bZMckEsYE5QBEJGOg1ilQd/HPKQUIbBly5MlSKwU3vzogIEBBgwYhek1SJUEZYUIgYxWyBMpARooCYPESwvOuBNFmBMQXJCkxqqcJemU9ePVSpQgOWLBA9bcuLnepRbOSYAqq0uOCSAlu3G0N6WyaVIgtAXouDmIhAk8wOqbqwMsqSzle+sGVABwuXKlyxUsFBjwHHpBkTBAVwGB5dFeJQkRxVRtpZGFd8Y1kEZ/PmSooQ8BDkigRhEo6JU0JwABnmv5AVbGE099dwQYG8boAxZifAjULb6BJc8RjkGw2gIB/AUYW2W4950QUgAhY/+MXVBwno0aYZBYb+I85BgP2bUzJABOUBFAAi1CtsASMC65IRa3QZkQCWLIFJMTPDQwhmpYNvDBkAQskUUAaKAhRRSsCdGAABqSgUUbS4JRAQtq3qMMBqQlCFkDKbRFwBFSPHGEHBUgUAEVR2SBJV8JoFCGhieYgKiMYBTAaKPnYNAVMnb9Ng5kUhxx6Z5P/JEhSxmCIUcSV47awBJOaFjCCVcsSYarsMZKazgKjnPEcXvyEAAQXZgJBgVbAMrDmEcs0YOZMT7bRbTmyEprQB/pci1rVSSLboYXIDHFEW4sMRsV92rIhRgesssMpNOOI2IQk0rha8BtnJAEEhBAkAD/BEv8AcQQQxQhQxAyUhCBwe3O+lUQCR4zL19CVHFqwBlykYZTQgSwQ7kB8JCAEEjAECMXGJHc7rvggLMyXwFUAHOGfywhbnF8IbHECRq2sajQJdeqy0zW8jVGFluQoCEWH4BsZglIGMfzuRx+8SrW2bgb6bvXLoBWA1t0myEFJoiAbhlpH8eD1B8U4AQRA8CNDgZtUnuUvOOiFUXYG5Kx6pJoHxeF1DR44QUSYTgRB0qKZ2PAiDHtGHlJeDe7tA+As9xAAOxMscZsSOSexA4dlI7NDXNI1JCt4TDMVxYvw1zBE8WWJFVyc5KUXHJhTCECBoP5zkwMid0lThB2NxgF/xuvVyAFBH1B8MakSCin7xFNhKD9LyTEMCuV4RwR/qhZPKF0wBSYgrjsw5fkgG4NYaBC7+bnCxb0RhoEWdl2pLAEKqSBC2QwUxvAwAY0oI+Aj4FAcrzghIIxkBUMAAjX5NWU2VEBMxB4EALAoDcNkQBcXgDhd6qQhgyc8BcjQJBvgnCEmkklDAwSIRrYgAAsOJELFQCAFNJGIR1Kjgr1+OEvUig8Ih5BKlNQQl+EgIYpTGULVHiCF6JQRSsaawtx0CIwLFCtIxRhCUuoWBtLor4oACpMbqTZ7C4gPzkCowNcnAkBniDGPQrOjXyZnRS2UABDOsMDESgCREwDSEgSEP8CDYjCE9IgBgNkz5LNOMARpAE1T4JQChR0Ag5CkEVUXoMBWdhZAzppn5xUIQtRqMIvqxDMPVVBPhU0gBgGQAIT2vIXPEpAFJZQHwKuJgq5coMd+bXNIrjhjkt4gqbS9Ex08KhlU9CTIyX3hCI4wATwjKc84bkEz61hCqcspzlE0ADkhCGUT/jgcZ4wz4KaIAfwDGMYqhdHfZ6jA0v4EpYg4A49jjEKRxgCEeSJUITqAZ4I9YL7AnAGh6LjAGLwQgCmUARMHgEJ/SQVGpJA0yRogAg4hQE8P/pREwxBOUqAQDlMig5l1uEnLBBBGNbXmgDU9Kk0nSdCTfA5i1WhDkT/zYgHGEAFJVAoAUiA6lM1oNOP6rQIa8BSAoaa1YSMAFNVfIMXxEpTDZigp/B0zIQCwIC2ZoQEBsgZXyAwV7HadZ7lokIuA/CkTJCgkH4lRQyOgALvCMFHs6FN7pAQgM56trMNiGkRNMEBUEE2sqKAAhWaUkX0VYw48GnRexpwAUxYgAMBaApbUSsKEhSARet85M6m0FhKRGAKqklAkIqwAwZAoSW8BUUcWKTD0MZpAfi8hAikUBY4PMEBDVhAAo7FgeiGQgyCtQ/PcqcEGuRTEjZIQmV5AIclTAFMxImCBBpq3k6wwAl9IGADNquE0VrinDrLFH6xBAfazaC/nxjB/xOqq6/EWaII+2vlauxqxzPUEsKY8IANjsAg+4hwCcWdRBGaJ74jTAEOMF7AE+ACYkuQoAV5eOcSQDjgJ6RYEhLw0WOKgAI4EAcCU7DCh2sMiQw4gAg12IEDjsDGI8mmpZeAQjVJ1UoevGEKElgGkx9Bggk4gAZGqIERdmCE7hyneUp4MCY4UGXICCELX2RCjcYMCQzowQ1r3kENauCAHUgBkHc+ghdu14A1yPkSB9gxa6TphhdAl8+PKEChd8DpTjtAsVCr2Q5MoIYp0CbOmqiDkViDhBO8jclQeMADYh3rWdt61iwYgR4coIBO+9oBRZBCP8drBBl8gNQGBIEm3v86KsgoYQlncKY+m0kCKBjABjYYQAF2bYJ39vSjU8WrA6Lsa04bwQEv8FIDnrCCIgigCGpon4EF4RxpLyICaFgnYWXwapMeIAMDEMAKckwDQo/bCL0ut8IVwGsHbFrhO5ABDpywBQes4AQYl8ER1pCEQZCAAysQwAjsnQgnxDSESNjCe/XZARnAwAFGiDmbIU5zT3NMygpIuMJpIAIKvPsERQD6CpYjCBJoIehF0AA5JaE/gfIlDEdQNlEzIIEoB7rmWMd5pxk+BAUMwddrroEJKNADoP/8A0mQgHOO/vMiWIF0kvjAfNO3nEqaNAMn0HnWa35uXvua4X5feBGYEHT/jLtbDS9gwgt+AHSzf+C0kFClkfuyuSMs2ZYhkMDD907zNZsA4grIMejfKYOfA10GjP+BDBoP9MdXwgqI5oFySuhQEhBg85ynucP1vnWG03wINFh90AWO8ROsoPRIb0K/HaGDARCLZsVxnwhIzsAIwDz3Wc8573/d9a/vIOFd10DpT/CDiwddA+4GOhPKK4kIUKACcpDida7TJy9QYQQm1Tz2sQ74rDug64C3ezl3fCeAfmanB2ZHflogCQdQAALTKQCQBmkgB1uQBiLnUNa3ffu3dTmggeX2clznADnmcCZwceInAEJneEXwASPTZGeAAxn0K+viRE9EATYwbVXn/4H7ZwTdtncCmHOdZnBDsAJ6oAewIANI925WsHyLcAZnRgEB0wUCUk4jkAMIt4Ggpwecx3A6qAA0wHArsAIfgII/UAQSYACQ4AEgoABpJgOuYybPwoRaxAEFh4Wjp4Pf53B7F2hDcGZhuAd14EOQAAXjxmk5cAJmYyZ/sECoRIh2qHtAWHPah31f53Ur4AJw9wgTQG6cRmgnUAFvuCEUsIC2ZABW+IjlVgMdmH15gIe/pwAyYAU2cGmMYAPX12kxlwM7cAJl8AdcwAUIIAI9YACZaEgCwImoyGl66H+4h30xVwMKQAAtQAK0eAg3oH8Ql2YOgIQeQwMOsHSG1AEKgP+MqMiFe2eOqIhwCvABICBtWlCIfDeJNUAA5XSMG8hwhGZwaJYDzXiHrriHhGYFU2gIGFB1++cAE/BMLHCLuWdxDAACDMAA2GZ8/LiFgZeMnfh9BGADz/EAt7eDCnCDttQBenB1nHcHs2gIK9CPoNd9GImL0KgAZzgIa2iS5ygBjGhJH4l9DsB+hRADP3CQLImRg0YAA9AEgraDO0CKjTiUEEdo+FcIOnAHNpl97+SUjxhzaYaFDMeUlvQFaMaTJTAgByAB/7hw8DSJL4mRaSYBxShHElCVNTePD1AIJcCGcpl7I4iVa5l1NYCTz2R9zngHMVAIZyAD5GiHOXeRfbn/gfNYTnqQmJ3nACI5CCUgc33JcGqgZo3pjNFYjXIUAd64hRKgA4RAADPXmFy4jGfZl3+pT0VQAT1gApxJc9C4DCxQACdwB5K5lovJl53Zk+V0Bj5wBWTQAzLgcNC4cBzwEwYAj52ZipoZZa2JitD4llpEAjjgA/5xBX/ABh+QnAfXawxXmH8QA3eQmtE5esCZjL0mA6D5QyNwBgXgATFTnACCAyfQcA6XAm3wBxiwkuuZfQz3eb35kg6AhoakDyJwBEewBVhQJhnSBVyABVegmycgAyawAee5AmE5oPw3ajLge9GJcNipPVwlBUWWAHPlBGAgoT5AAv/xiwUgAxuw/wFqUIcgeo4VUADutJxreW52p0UtEABwQCfjkhxEAAB/AKP/8QcVsAEqkAIfuqNzaQIP4wN5B53JaAR6sKBp4CM78xSXpQRzJQcU4ET/gQUIIKUpoABuYKX+lweVUwEjWptZqQAJKUdNEAWzwx0nxycxNFNJAABsAAAVMAMukALtuaNYuiH2iQVAYAI5l5ec5wCmqUXolSl4dEdfkgAYVhJKEAXK8QR+MKV5cKByqownYAbOYgUcoHlqZqm2KQByqDgh0B2dNQZwUAVUsAPcUQSTx0dhsAUqsAFcuqpoCYVLggWFOQItQAD4aHV7mAMr5zsegAc2QTOkmk5PMGqIJv8FLqACH9CoIGoCVLMkXVAAp8QCEVAAVqCcePqUe6pFeJBLPFAxU/AELwYHaKAGw0oSx/ICHwADysp/DpClMWIGFfBj1lYAE4CPcplzLahFUJAG0pNO9yUF6eQAEsVHU2ACGnCwfGcC/6OuFXCtggAFGGADBJBj0Bho5xYB1Ic1F/AEb0AFNHAaFJQpXyRb+EGpJEtzOSAAl8MqFrYIFtABBZAC5TpuQyADtmRt5XJfCeAEEyJNWSBQSEIE1QmiYscF93IFPskIHpBrEWAABMABgmhLEiBRCRBRsSU+RfC155o83lK2kRefcuQEZKEaQbJgfHQEuji02WgCYosuDHv/qxAWWG8gRhDgAEtwGoIie1TQbbS6qmLngPdCBrWFaYTAAC4mVEVQBXhkO0pwuXY7oEUbg+jSBXgAuoUABWIgAkTQAHCQBelEBVPAj6qqrKqosGbCBQoqu4RAAkQwBhezBk9AA93nfYbra0ULo2ZSARVrvIMQAU8QVlpwATtgsNGrcNuIBTCzuNhrCB1gAPWAAVUavp1msktjBn11vrphrju6jYkbMFcgdfR7CAzpvmxmAhdQQ/diB3zbv8kaviaAA9QLh/Pbv/4Lve6rADkgvOiyvxCMCF9gv+uZAwQQivdyATUruwfwv4Yrdn/gqjBzBeCYwYSwwROcAxfgugGD/wAq68IeYMLAawIIYJ+v88AufAgYMI6G6wB2QMAw82hBfAgtQJUHm2YmkAcXcAVmoMIhPML9GwHpCbyweGzxJAMXUAG/0Sl6cwVOYAIHvMSC0AIwt7r7l3MnMAE9IAACwAQroAYFtQLrCE9DkLRqjAjWB3OZi4rbOAEE0AMEcMg9sMiLXAIC8AEZOnhjOAQ3/Md/QAI2IAAHF501UHqHLACfnMiinMiI3AOGvMgCoBmWrAgZoAbjRsQvCY2lZ8iHLMqIbMujPMo90ATXu8qFQAIMkKHL+Lv+V3q3jMuknMtMUMq2LAFYnMERcAIEkKFc6MbfpwBMUAagzMzJXMumzP8EKHjLiCwCJ1CZvvzLDPDJJ/C0q5tzH1DLuXzMo/zIJyDPnywCvXzOgvAKcmzIGZqc84qwyzzH3qzLtWzIISfO8DzNqqzPhDACTIDM/5zAC7eOCm3QozwBdLzMuZzMPYDPDn28wXzME1DS6yyeMRmEeegA0iwCBq3QiKzRTCDNHr3QiSwC/BXSMUDTC43IGfoBOScDGtBwOccEMhDK9pzLE3ACM23TLz3N/BvSUMAAAnDMJD3HxScDxjbT9SzHVY3LzGzKdPzOMF3QudwEYhbSA1AE4rzNBm3IAnDKHe3UBODPXG3LoNzRYS0ATVCXIf0HGRDRn2zV8TzHBB3WuKz/0e/2AUGHzIhtz3MsAMz01wZSzxht1sgcz4m80XuQAoknx7qc15r9yf4MeecczZkN2aTs1sy80VowACzgHFNt2fD82Hg9zlbx138wAk3w1d2M2YY91wLAASPg14NwAL1N0KGs13MN0n+tmzat2k89x0yQ04RgANJM2Ihd05/cBJe3yqj928BN12Xnx4Sghr6t3KN92wLgXCENzLRN3pp9y0vdBIlwACKwzV+93Zh9yCeQz6u81nJd0Nqt2bmtGy4915kt3k0QlfrcAU1AAPmt4Krd1lehCAWw3wQO1mBtFc+MvVS94T1N14nMBMV7CHSh4OJt03Xs4OeM3XN92PNN/8pld+KHYADCWNsjXuA9oAUuvspQEOHbXeChjYI2fggRwNHL7dT2LAJF0MKWjN1VLdoL7s1u/d9m+86DveMcXtdFcOR/DNFlTeTdPMcMwLiCwAG+fdkrXtcEoAUnGsQixtPr3eZ1XMn0JgEiPt5WzQQ5uco4btvcTeACQAffTZCCzeZOTcsnYN6r3NsxXuebLQB4Tm9fcAHizc0bPsd97dAYQNuaHureXHbmvAhWoOHSrdcy8OeWjAFKruhJPc1AfG90Puj8LdkfDroewAGWLepETuqPIGI5zuSR3gMpkOug+wBCLtqpvtoH/ggSEN++vtB1jOygm+F83uUf7dyOQP8CMjDgVZ7ME/ABP77KLjvhKx7rBMAEbdsIHvAF9czaHK7QJ4BVDp3J2V7TpXwCad0IIyADSa3uoCwD7b7KLADpzC7poiwABc8IHDHom17XAgDmanwAHL3f8q7v/j2QjnAAH5Dg4R7PVmDtYxZwVU3Yir7wdBDnKD4D2szNgl7KMsDyEGx0Ep7aXF7LJ0AHaG4IZ1AGKr7w8gzn5xxwwV3lkD3lA5DGhzADY67j3vzl53zweX30tt7T7C4JJM7mEyADpr3EA5DdCi/dTFDui3CNcl3WEi/OZe/LLJDOEw7z+U4A7c30hvDvSY/XeH3hAQ7qVM7cBs3vkgDvac/dVo3/5UCe3PMO9T3d6HZfCB6gB+rO3F1v9i6M3TJO4W6dzHX89Yvg7U9/60xg3Goc5NEt4ihv5o9fCDYQ30Ffy3UP6Dw9+YrO7cHO6xE/5I2+ygegBTev7f0d0+09CSTwAYg85a9/yG1vyXGg5AH/9/IsACLA6o3g7QMe+qKsBZWOvWHP4xpf0BcQ1ZCA+Xr994k8AUXQ70EM4Vaf/PNs35PgAWcg9DqO2OhP/TUfzPX//YgNCAIGf4SFhoeIZz0Eiz0CBIyPkJOOTFCImJmam5ydnp+goaKcN0yLk6iOqJGpWiSjhBEip5C0jYyTJwwPsL2+v8DBvQMnuLenx7W4BCI2/74RxcvKq7hMEcLY2drbnyNNrNThqQJWvyAntqnGkBMCGdzw8fK/JMRlj8jr+bcEFwO/NCgpozWuyY15CBMqRFRKnThwlMr9kkFQ1cBIPU4MWsixIzxiPSou43dMQJNLvs7YmbaOWo8iGz3KnNnrwDcR+FxKEgfil4cvqwjqIzCByUGaSJNy8gBSHcmhjCzR+8DvIjWTKJVq3frnBtWROx/WOvHFw68OHxw6HXuNq9ukIyhSEpDs6SkmI4B5GDFEBjpJE4QuKhrjreGZHeSKHUeprDCzP34wmUzgkQDLlU3mPcx54YEzTE5cRjYBKjIrLLLpODLhRBEmDlaskCF7xf8HK1Zidt49zwMLA1Z+yJBRBDNRdsjJaityZUKJ588v9Aj8wYdZ3tgVavlRpC8NEwqKf7hM/oQIDgeyOdhBxocPM+/b+PhTgQKdV9nzy8tgpwIWLBSw4cQJThSxQxG0SaCFFUdhw4AGMAwBBhdXmNGFD/KBAYAeDOCn34fCHICBARXAB5977mGBQAUEGKCbgxoQocEPJphQRAFBABAEG0PAAKEVHYAopC8YSCBcEUcAQAEFXHRxRRddmHHFCbxsY4MGQ2CpwZY9wkAEDD7+MISMEgxQ5ZBodpKBHlvKCCYMJpyAQxk4iMAGDTA0ocUBcWADhYyRBfoDEYIW+sOMTaT/l+aimWhBqJhDHDromxCGuaUMNfQZTAyHEpplloWCGiihRGihA6OoFnIAoD3OOOqMomZJKAx3hABMB0QAKuajhfL6KhESvJMqozHEGJmvvQo65g8wOBDCmaM04aqhYk4bqKiHaiBBW8OiWcCgnVIbWaTXdoqlFlmJIoGx1H5q7biCzmhmt0KSYIWW4Bo6JruSBrplmbBI8Ciy4l6L76FDNPEPvfphoAEN2B7r6sCPkmswEQwwUEAHI0CriZEz6lrwrvpmm3AMDTLMmQdNOFAxrwSDy++x2cq4wgtfYNDJuhUXbPHIMmIpAQMDFKayYR7IsCWkB48spo9j8rovDQ4w/wGCx4dYoSuoXG85c7u6thmjFQxwe7RSD6ww8Mj7RrZDvhJffKgDBNjg4SEsfxB0uMZ+sELETI87baQDY3n2Vg6LLLGMn4oMONiRwfmFooj4NoABNHM5AgkH3Islq77+XOiMqR2eFAbZeh0jEVZ8YWRkqzsd6oyuwvCB2ZiMcIDuDDSx2R8H9J4t7DG2GTS+W35hq+lJedBBCB1g0EH0GYSQ3gNxbCdBlksX4eXg4g6RpQMmfJHuUpU/ID30AwgcqAzLjgnDDwxcx3x2vHggBgOAhvx0xYN7lAP08ILzCeMBHiCBBzyQPw/UgWj3S9MBDvCADkjgXjUD15esRb4zYCwtgiAMRggM0IEHqW5LfNAAH2BghQgIK4QwjKEMZ0jDGtrwhjjMoQ53+IdAAAA7\" title=\"smithers\" alt=\"smithers\" ALIGN=LEFT />";
	private static String error = "<img src=\"data:image/gif;base64,R0lGODlhHwAgAPcAAAAAAAAACAAAEAAAGAAAIQAAKQAAMQAAQgAASgAAawAAewAIcwgAMQgIcwgQcxAAORAAYxAIABAQaxAQexAYaxgAQhgIShgIaxgQQhgYaxgYhBghayEQISEYECEYISEhYyEphCkpYykphCkxYykxhDEYYzEhazEpYzExWjExYzkhSjkxazk5hEIxSkI5WkJCWko5GEpKUkpSe1I5KVJCUlJClFJSSlpCGFpKCFpKKVpKSlpalGNSMWNjnGtSIWtavWtrnHNjUnNzpXtaIXtjWnt7pYRrIYRrMYRzMYR7pYSEnISEpYxzMYx7GIyErYyMpYyMrZSEMZSMKZSMpZSUpZSUrZyMKZyUKZycpZycraWMEKWcIaWlEK2UELWUObWlALWtGLWtIbWttb2cEL2cSr21vb21/729GL29vcYAAMatCMa9AMbGEMbGxs6lAM6tAM7Gxs7OIc7OxtatANa9ANbOxtbWvdbWxt61AN7OnN7Wree1AOe9AOe9MefGAOfGIefGWufOAOfOUufWAOfWEOfWa+fWe+feAOfeUufeY+fea+fejOfnAOfnIefnSufnWu/GAO/GGO/OAO/OEO/WAO/WCO/eAO/nAO/vAPfGAPfOAPfWAPfeAPfnAPfvAPf3APf3Mf/OAP/WAP/eAP/eCP/nAP/nCP/nEP/vAP/vCP/vEP/vGP/3AP/3CP/3EP/3GP//AP//CP//EP//GP//If//Kf//Mf//a////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////yH5BAEAAGkALAAAAAAfACAAAAj+ANMIHEhwoJ1FBRMqXChQj6xYoOAwnJiwDqZPsGDFWUKxYxpHjFjVymijiEeGdhgxmnVLFiw2E8qcVIjoEKZZtmhhjLFjZkKVnx7WYgXrjAM0PgfWvPQQ1qxZRGP0SJqmzqFDrGSxWhOLFtEwE5D6TDSokywuAAAEaCQLFasXQJJeRSUrQAAGBnLQcgu2zUyylh4CEFDhwIynqFC9EDKTUFmXEQgU9gFrL6stGvx2LBTIkiuXOAxYqKCl8qtOqFKY7DiorCtUsL4UMKEiEKtWtEqhuqJB4kTOlFS5KgWLDoEaLVihKkXL1ShOKZJQJBSIkitTukt5+MGDValSrmj+cUIVBYRmhYb8BDKl6jvxGwq6eP9OyxSnUiE4LpxUXZWpUd/B0kQCb3g3yiivvLJJKVKAIAd6fkhCiioHHoiKGghAgkqFptBCyiiUhPCEQpNE6F+FAG6CAYAoJrhgFCQ8SFB6kEyI4oGljFEKiqKQUospoowiYkF++KGJf6IkaaFb340SZJKr0BKkFQ4OZAgfNZ6S5JalqCFAAB48ueUoHoqiSQhKDBQJlqeQsmWSpXQAwAAGDIHKm6KcsoomoyARYxp58MFHKFriOcpgJVxAxJ1vkrKKKKFAksEUaQiCBx94cgnDABCYQUYpmbqZiSg6sJDGH5dCksmqrGYSiihiRgThRSiatMoqJJCIwoQDYgAyxxx4BCussHy4CsmwyO4BSSg0UJBmH3O4Ie201FZrrRvAHuHABpSmgcUKKYQr7rjklotCCtvK4FsaZTzxxBRTUEFFvPLSC2+98cILbxYPBgQAOw==\" alt=\"error\"";
	public static void main(String[] args)  //these are the files to be processed
	{
		HashMap<String,ArrayList<String[]>> map = new HashMap<String,ArrayList<String[]>>();
		int markDownFiles = 0;
		String commonSubstring = null;
		for(String arg:args)
		{
			File f = new File(arg);
			if(f.exists() && f.getName().endsWith(".md"))
			{
				if(commonSubstring == null) commonSubstring = f.getParent();
				else commonSubstring = commonDir(commonSubstring,f.getAbsolutePath());
			}
		}
		for(String arg: args)
		{
			File f = new File(arg);
			if(f.exists() && f.getName().endsWith(".md"))
			{
				String parentDir =f.getParent();
				if(parentDir == null) parentDir = ".";


				try { 
					Scanner s = new Scanner(f);
					int type = 0;  //unknown
					String relative_path = f.getName().substring(0,f.getName().lastIndexOf("."))+".html";
					if(commonDir(commonSubstring, f.getAbsolutePath()).length() < f.getAbsolutePath().lastIndexOf('.'))
						relative_path =trimPathLeft(f.getAbsolutePath().substring(commonDir(commonSubstring, f.getAbsolutePath()).length(), f.getAbsolutePath().lastIndexOf('.'))+".html");
					if(parentDir.compareTo(".") != 0 && relative_path.startsWith(f.getParentFile().getName()))
							relative_path = relative_path.substring(f.getParentFile().getName().length(),relative_path.length());
					String link = "<a href=\""+relative_path+"\">"+f.getName().substring(0,f.getName().indexOf("."))+"</a>";
					String uComment = check+getTitle("Demultiplexing was mostly successful!")+"Barcode demultiplexing was fine. For details see: "+link;
					String[] report = new String[12];
					while(s.hasNextLine())
					{
						String line = s.nextLine();
						if(line.contains("MD5SUM"))
						{
							type = 1;
							report[0]= link;
							report[1]= error+getTitle("Information not found. Rerun fastqc.");
							report[2]= error+getTitle("Information not found. Rerun fastqc.");
							report[3]= error+getTitle("Information not found. Rerun fastqc.");
							report[4]= error+getTitle("Information not found. Rerun fastqc.");
							report[5]= error+getTitle("Information not found. Rerun fastqc.");
							report[6]= error+getTitle("Information not found. Rerun fastqc.");
							report[7]= error+getTitle("Information not found. Rerun fastqc.");
							report[8]= error+getTitle("Information not found. Rerun fastqc.");
							report[9]= error+getTitle("Adapter trimming results not found!");
							report[10]= error+getTitle("rRNA filtering results not found!");
							report[11]= error+getTitle("TAINT contamination checking results not found!");
							
						}
						else if(line.contains("Only") && line.contains("reads!"))
						{
							report[1] = bad+getTitle("Total number of reads below 10 million!")+" "+String.format("%3.1fx10^6 reads!",Integer.parseInt(line.substring(6, line.indexOf("reads!")).trim())/1000000.0);
						}
						else if(line.contains("Plenty of reads"))
							report[1] = check+getTitle("More than 10 million reads.")+" "+String.format("%3.1fx10^6 reads!",Integer.parseInt(line.substring(20,line.length()-3).trim())/1000000.0);
						else if(line.contains("Read quality was: pass"))
							report[2] = check+getTitle("Read quality passed fastqc thresholds successfully.");
						else if(line.contains("Read quality qc"))
							report[2] = bad+getTitle("Read quality did not pass fastqc thresholdsholds!");
						else if(line.contains("Non-uniform distribution of nucleotides across the reads!"))
							report[3] = bad+getTitle("Nucleotides are not evently distributed across reads! (expect 25% of each)");
						else if(line.contains("Cytosines have been depleted... suspected bisulfite sequencing."))
							report[3] = check+getTitle("Cytosines are underrepresented across the reads. Suspected bisulfite sequencing run.");
						else if(line.contains("Nucleotide distributions looked about even!"))
							report[3] = check+getTitle("Nucleotide distributions looked about even (25% each)");
						else if(line.contains("GC content was non-normal"))
							report[4] = bad +getTitle("Distribution of GC% did not follow a normal distribution as is expected from an ideal sample. Possible contamination or GC bias detected!");
						else if(line.contains("GC content looked ok"))
							report[4] = check+getTitle("GC% was approximately normally distributed. This suggests low GC bias and clean samples from a single species");
						else if(line.contains("Read N content did not pass qc"))
							report[5] = bad+getTitle("Reads had too many Ns indicative of sequencing errors");
						else if(line.contains("Read N content qc looked great"))
							report[5] = check+getTitle("Very low N content suggests no blatant sequencing errors for these reads");
						else if(line.contains("Reads are different sizes with reads of length"))
							report[6] = bad+getTitle("Reads have a distribution of sizes suggesting substantial trimming");
						else if(line.contains("The vast majority of reads are the same size"))
							report[6] = check+getTitle("Reads were mostly one length");
						else if(line.contains("were not duplicated"))
						{
							double percentage = Double.parseDouble(line.substring(3,line.indexOf('%')));
							if (percentage > 50)
								report[7] = check+getTitle("Read duplication was ok >50% were not duplicated")+String.format("%2.2f%%", 100-percentage);
							else
								report[7] = bad+getTitle("Read duplication was excessive: >50% were duplicated")+String.format("%2.2f%%", 100-percentage);
						}
						else if(line.contains("Overrepresented sequences make up a significant portion of the reads"))
							report[8] = bad+getTitle("Fastqc detected too many over-represented sequences in the reads.");
						else if(line.contains("Few overrepresented reads in the file"))
							report[8] = check+getTitle("Fastqc did not detect too many over-represented sequences.");
						else if(line.contains("reads too short"))
						{
							double tooShort = Double.parseDouble(line.substring(2,line.indexOf('%')));
							if(tooShort > 10)
								report[9] = bad+getTitle("Adapter trimming resulted in the removal of more than 10% of your reads. Adapters were frequently sequenced as part of the read.");
							else
								report[9] = check+getTitle("Adapter trimming did not remove too many reads (<10%)");
						}
						else if(line.contains("were classified as rRNA"))
						{
							double rRNA = Double.parseDouble(line.substring(line.indexOf("rRNA")+6,line.length()-2));
							if(rRNA > 10)
								report[10] = bad+getTitle("rRNA contamination exceeded 10%. You may have lost a significant portion of your reads during rRNA filtering");
							else
								report[10] = check+getTitle("rRNA contamination was minimal (<10%).");
						}
						else if(line.startsWith("Organism"))
						{
							ArrayList<Double> counts = new ArrayList<Double>();
							double total = 0;
							String dominantOrg = "";
							while(s.hasNextLine())
							{
								try{
																
									String[] next = s.nextLine().split("\\s+");
									if(next.length < 4)
										break;
									String[] combined = {"","","",""};
									for(int i = 0; i < next.length -3; i++)
										combined[0]+=next[i]+" ";
									for(int i = 0; i < 3; i++)
										combined[combined.length-i-1]=next[next.length-1-i];
									next = combined;
									double per = Double.parseDouble(next[2].substring(0,next[2].length()-1));
									if(!next[0].contains("Ambiguous")&& !next[0].contains("Unidentified") && Integer.parseInt(next[3].trim())>= 25)
									{
										
										if(dominantOrg.length()==0)
											dominantOrg+=next[0].trim();
										else
											dominantOrg += ", "+next[0].trim();
										counts.add(per);
										total+=per;
									}
								}catch(Exception e){}
							}
							if (counts.size() > 0)
							{
								if(counts.get(0)/total < 1) //Report >0% contamination. We ignore Unidentified, or Ambiguous, or one transcript 
									report[11] = bad+getTitle("Detected "+dominantOrg+" among sample reads. Check sample "+f.getName().substring(0,f.getName().length()-10)+" in "+parentDir+".")+dominantOrg;
								else
									report[11] = check+getTitle("Reads are from "+dominantOrg+" only.")+dominantOrg;
							}
							else
								report[11] = bad+getTitle("No specific species detected! You reads are non-organic or from an unknown organism")+"No species!";
						}
						else if(line.contains("## Undetermined Reads Profile ##"))
						{
							type = 2;
						}
						else if(line.contains("Check that the right sample sheet was used during demultiplexing."))
						{
							uComment = bad+getTitle("Demultiplexing really struggled")+"Barcode demultiplexing failed: "+link;
						}
						markDownFiles++;
					}
					if(type == 1)
					{
						if(map.get(parentDir) == null)
							map.put(parentDir,new ArrayList<String[]>());
						map.get(parentDir).add(report);
					}
					else if(type ==2 && uComment != null)
					{
						if(map.get(parentDir) == null)
							map.put(parentDir,new ArrayList<String[]>());
						String[] uComments = new String[1];
						uComments[0]=uComment;
						map.get(parentDir).add(0,uComments);
					}
					s.close();
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		}
		if(markDownFiles == 0)
			System.out.println("Did not see any markdown reports!");
		else
		{
			//lets try to combine projects with just one sample
			HashMap<String,ArrayList<String[]>> newMap = new HashMap<String,ArrayList<String[]>>();
			for(String parentDir: map.keySet())
			{
				if(map.get(parentDir).size() == 1)
				{
					//lets try to combine with above dir
					String gParentDir = (new File(parentDir)).getParent();
					if(gParentDir != null && gParentDir.length() >= commonSubstring.length())
					{
						if(newMap.get(gParentDir)==null) newMap.put(gParentDir, new ArrayList<String[]>());
						newMap.get(gParentDir).addAll(map.get(parentDir));
					}
					else
						newMap.put(parentDir,map.get(parentDir));
				}
				else
					newMap.put(parentDir, map.get(parentDir));
			}
			map = newMap;
			for(String parentDir: map.keySet())
			{

				File projectSummary = new File(parentDir+"/smithersProjectSummary.html");
				try {
					outputSummary(projectSummary,map.get(parentDir));
				} catch (FileNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
			try {
//				if(!commonSubstring.endsWith("/"))
//					commonSubstring = commonSubstring.substring(0, commonSubstring.lastIndexOf('/')+1);
				outputOverallSummary(new File(commonSubstring+"/smithersSummary.html"),map);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	 
	private static void outputOverallSummary(File file, HashMap<String, ArrayList<String[]>> map) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(file);
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		pw.println("<!DOCTYPE html><html><head><style>table, th, td {    border: 1px solid black;    border-collapse: collapse;} th, td {    padding: 5px;}</style></head><body>");
		pw.println("<h1>Smithers Run Summary</h1><br><h2>Summary for folder:"+file.getParent()+"</h2><br><h3>Ran on "+timeStamp+"</h3><br>Click the links below to see summaries for individual projects sequenced.<br>");
		for(String project: map.keySet())
		{
			String relative = "";
			if(project.length() >commonDir(project,file.getAbsolutePath()).length())
				relative=project.substring(commonDir(project,file.getAbsolutePath()).length(),project.length())+"/";
			pw.println("<br><h3><a href=\""+relative+"smithersProjectSummary.html\">"+project+"</a> with "+map.get(project).size()+" samples tested.</h3>");
		}
		pw.println("Smithers is a QC pipeline developed at the Salk Institute IGC core.<br>"+smithers+"</body></html>");
		pw.close();
	}

	private static void outputSummary(File projectSummary,
			ArrayList<String[]> samples) throws FileNotFoundException {
		PrintWriter pw = new PrintWriter(projectSummary);
		String timeStamp = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime());
		pw.println("<!DOCTYPE html><html><head><style>table, th, td {    border: 1px solid black;    border-collapse: collapse;} th, td {    padding: 5px;}</style></head><body>");
		pw.println("<h1>Smithers Project Summary</h1><br><h2>Summary for :"+projectSummary.getParent()+"</h2><h3>Ran on "+timeStamp+"</h3><br>Click the links below to see details about individual files tested.<br>");
		boolean drewTableHeader = false;
		for(String[] sample: samples)
		{
			if(sample.length ==1)
			{
					pw.println("<p>"+sample[0]+"</p>");
			}
			else 
			{
				if(!drewTableHeader)
				{
					pw.println("<table style=\"width:50%\"> <tr> <th>Sample</th> <th>Reads</th> <th>Read quality</th> <th>Nuc Freq</th> <th>GC Content</th> <th>Read Ns</th> <th>Read Lengths</th> <th>Read Dup.</th> <th>Overrep. Seqs.</th> <th>Adapters</th> <th>rRNA</th> <th>Organisms detected</th>");
					drewTableHeader=true;
				}
				pw.println("<tr>");
				for(String s: sample)
						pw.println("<td>"+s+"</td>");
				pw.println("</tr>");
			}
		}
		if(drewTableHeader)
			pw.println("</table>");
		pw.println("Smithers is a QC pipeline developed at the Salk Institute IGC core (C) 2015.<br>"+smithers+"</body></html>");
		pw.close();
	}

	private static String getTitle(String title)
	{
		return " title=\""+title+"\" />";
	}
	
	private static String commonSubstring(String s1, String s2)
	{
		int a =0;
		while(a < s1.length() && a < s2.length())
		{
			if(s1.charAt(a)!= s2.charAt(a))
				break;
			a++;
		}
		return s1.substring(0,a);
	}
	
	private static String commonDir(String s1, String s2)//lets assume pathway separator is always /
	{
		String[] del1 = s1.split("/");
		String[] del2 = s2.split("/");
		String result = "";
		for(int i = 0; i < Math.min(del1.length,del2.length); i++)
		{
			if(del1[i].compareTo(del2[i])!=0)
				return result;
			else result+=del1[i]+"/";
		}
		return result;
	}
	
	private static String trimPathLeft(String path)
	{
		return path.substring(path.indexOf('/')+1);
	}
}
