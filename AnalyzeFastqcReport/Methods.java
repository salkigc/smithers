package AnalyzeFastqcReport;

public class Methods 
{
	public static double average(double[] list)
	{
		double avg = 0;
		for(double d: list)
		{
			avg+=d;
		}
		return avg/list.length;
	}
	
	public static double std(double[] list)
	{
		double avg = average(list);
		double std = 0;
		for(double d: list)
		{
			std+=Math.pow(avg-d,2);
		}
		return Math.sqrt(std/list.length);
	}
	
	public static double stdM(double[] list)
	{
		double avg = average(list);
		double std = 0;
		double total = 0;
		for(int i = 0; i < list.length ;i++)
		{
			std+=list[i]*Math.pow(avg-i,2);
			total+=list[i];
		}
		return Math.sqrt(std/total);
	}
	
	public static double normPDF(double x, double mu, double sd)
	{
		return 1.0/(sd*Math.sqrt(2*Math.PI))*Math.exp(-Math.pow(x-mu,2)/(2*sd*sd));
	}
}
