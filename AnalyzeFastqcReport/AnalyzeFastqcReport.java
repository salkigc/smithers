package AnalyzeFastqcReport;

import java.io.File;
import java.util.Scanner;

public class AnalyzeFastqcReport {

	private String sep = System.getProperty("line.separator");
	public AnalyzeFastqcReport(String[] args) {
		try{
			//lets load in the file and read it!
			Scanner s = new Scanner(new File(args[0]));
			int reads = 0;
			System.out.println("FASTQC SUMMARY");
			System.out.println("--------------");
			System.out.println();
			String problems = "";
			String other = "";
			while(s.hasNextLine())
			{
				String line = s.nextLine();
				if(line.startsWith("Total Sequences"))
				{
					reads = Integer.parseInt(line.trim().substring(16));
					if(reads < 10000000)
						problems+="* Only "+reads+" reads!  "+sep;
					else
						other+="* Plenty of reads : "+reads+".  "+sep;
				}
				else if(line.startsWith(">>Per base sequence quality"))
				{
					String result = line.split("\t")[1];
					if(result.compareTo("pass")==0)
						other+="* Read quality was: "+result+"  "+sep;
					else
						problems+="* Read quality qc: "+result+"  "+sep;
				}
				else if(line.startsWith(">>Per base sequence content"))
				{
					s.nextLine();
					String lin = s.nextLine();
					double[] gatc = new double[4];
					int count = 0;
					while(s.hasNextLine() && !lin.startsWith(">>END_MODULE"))
					{

						String[] split = lin.split("\t");
						gatc[0] += Double.parseDouble(split[1]);
						gatc[1] += Double.parseDouble(split[2]);
						gatc[2] += Double.parseDouble(split[3]);
						gatc[3] += Double.parseDouble(split[4]);
						count++;
						lin = s.nextLine();
					}
					gatc[0]/=count;gatc[1]/=count;gatc[2]/=count;gatc[3]/=count;
					other+=String.format("* G:%3.1f\tA:%3.1f\tT:%3.1f\tC:%3.1f  %n",gatc[0],gatc[1],gatc[2],gatc[3]);
					boolean badNucDistros = false;
					boolean CytosineDepletion = false;
					for(int i = 0; i < 4; i++)
					{
						if(gatc[i] < 20 || gatc[i] > 30)
						{
							if(i == 3 && gatc[i] < 10)
								CytosineDepletion=true;
							else
								badNucDistros =true;
						}
					}
					if(CytosineDepletion)
						other+="* Cytosines have been depleted... suspected bisulfite sequencing.  "+sep;
					else if(badNucDistros)
						problems+="* Non-uniform distribution of nucleotides across the reads!  "+sep;
					else
						other+="* Nucleotide distributions looked about even!  "+sep;				
				}
				else if(line.startsWith(">>Per sequence GC content"))
				{
					s.nextLine();
					String lin = s.nextLine();
					double[] gcs = new double[101];
					double[] counts = new double[101];
					double total = 0;
					while(s.hasNextLine() && !lin.startsWith(">>END_MODULE"))
					{

						String[] split = lin.split("\t");
						int percent = Integer.parseInt(split[0]);
						double count = Double.parseDouble(split[1]);
						gcs[percent]=percent*count;
						counts[percent]=count;
						total+=count;
						lin = s.nextLine();
					}
					double mu = Methods.average(gcs)/total;
					double std = Methods.stdM(counts)/total;
					double RMSD = 0;
					for(int i =0; i < counts.length; i++)
					{
						RMSD+=Math.pow(counts[i]/total-Methods.normPDF(i,mu,std),2);
					}
					RMSD=Math.sqrt(RMSD/counts.length);
					if(RMSD > 0.02)
						problems+=" * GC content was non-normal indicating contamination or experimental errors. "+String.format("(RMSD=%3.4f)",RMSD)+sep;
					else if(Math.abs(mu-0.461) < 0.01)
						other+="* GC content looked ok and human-like ~46.1% "+String.format("(RMSD=%3.4f)",RMSD)+sep;
					else if(Math.abs(mu-.512) < 0.01)
						other+="* GC content looked ok and mouse-like ~51.2% "+String.format("(RMSD=%3.4f)",RMSD)+sep;
					else if(mu < 0.42 || mu > 0.58) 
						other+=String.format("* GC content looked ok but non-mammalian (GC=%3.2f%%) ",(mu*100))+String.format("(RMSD=%3.4f)",RMSD)+sep;
					else other+=String.format("* GC content looked ok and roughly mammalian (GC=%3.2f%%) ",(mu*100))+String.format("(RMSD=%3.4f)",RMSD)+sep;
					
				}
				else if(line.startsWith(">>Per base N content"))
				{
					String result = line.split("\t")[1];
					if(result.compareTo("pass") == 0)
						other+="* Read N content qc looked great  "+sep;
					else
						problems+="* Read N content did not pass qc  "+sep;
				}
				else if(line.startsWith(">>Sequence Length Distribution"))
				{
					s.nextLine();
					String prev = "";
					double lengthReads = 0;
					while(s.hasNextLine())
					{
						line = s.nextLine();
						if(line.startsWith(">>END_MODULE"))
							break;
						prev = new String(line);
						double count = Double.parseDouble(prev.split("\t")[1]);
						lengthReads+=count;
					}
					String len = prev.split("\t")[0];
					double count = Double.parseDouble(prev.split("\t")[1]);
					lengthReads+=count;
					if(lengthReads-count < lengthReads*0.75)
						other+="* The vast majority of reads are the same size: "+len+"  "+sep;
					else
						problems+=String.format("* Reads are different sizes with reads of length %s representing only %3.3f %% of all reads!  "+sep,len,(100*(count))/lengthReads);
				}
				else if(line.startsWith("#Duplication Level"))
				{
					String line2 = s.nextLine();
					double sum = 0;
					double per = 0;
					while(!line2.startsWith(">>"))
					{
						try{
							String[] spl = line2.split("\t");
							double val = 0;
							if(spl.length > 2)
								val= Double.parseDouble(spl[2]);
							else
								val= Double.parseDouble(spl[1]);
							if(sum == 0)
							{
								sum+=val;
								per=val;
							}
							else
								sum+=val;
							line2 = s.nextLine();
						}catch(Exception ee){}
					}
					if(per/sum > 0.5)
						other+=String.format(" * %3.2f%% of reads were not duplicated   %n",(100*per/sum));
					else
						problems+=String.format(" * %3.2f%% of reads were not duplicated!  %n",(100*per/sum));
				}
				else if(line.startsWith(">>Overrepresented sequences"))
				{
					s.nextLine();
					String lin = s.nextLine();
					double percent = 0;
					while(lin.compareTo(">>END_MODULE")!= 0)
					{
						try{
							percent+=Double.parseDouble(lin.split("\t")[2]);
							lin = s.nextLine();
						}catch(Exception e){break;}
					}
					if(percent < 5)
						other+="* Few overrepresented reads in the file.  "+sep;
					else
						problems+=String.format("* Overrepresented sequences make up a significant portion of the reads (%3.1f%%)  %n",percent);
				}
				
			}
			s.close();
			System.out.println("### Possible issues ###");
			System.out.print(problems);
			System.out.println();
			System.out.println("### Notes ###");
			System.out.print(other);			
		}catch(Exception e)
		{
			e.printStackTrace();
		}
	}

	public static void main(String[] args)
	{
		if(args.length > 0)
			new AnalyzeFastqcReport(args);
		else
			System.err.println("AnalyzeFastqcReport requires a fastqc report to analyze!  ");
	}
}
