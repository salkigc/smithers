package clusterUndetermined;

public class Methods { 

	
	public static double binomialCDF(int n, int k, double p)
	{
		if(n > 1000 && p > 0.1 && p < 0.9)
		{
			return normalCDF(k,n*p,Math.sqrt(n*p*(1-p)));
		}
		else if(n > 100 && (p <= 0.1 || p >= 0.9) )
		{
			//we are assuming that n is large and p is small so approximate with poisson
			double lambda =  n*p;
			return incompleteGammaQ(k+1,lambda);
		}
		else return binomialExactCDF(n,k,p);
	}
	
    /**
     * Incomplete Gamma function Q(a,x)
     * (a cleanroom implementation of Numerical Recipes gammq(a,x);
     * in Mathematica this function is called GammaRegularized)
     *
     * @param a parameter
     * @param x argument
     * @return function value
     */
    public static double incompleteGammaQ(double a, double x) {
        return 1.0 - incompleteGamma(x, a, lnGamma(a));
    }
	
    /**
     * log Gamma function: ln(gamma(alpha)) for alpha>0, accurate to 10 decimal places
     *
     * @param alpha argument
     * @return the log of the gamma function of the given alpha
     */
    public static double lnGamma(double alpha) {
        // Pike MC & Hill ID (1966) Algorithm 291: Logarithm of the gamma function.
        // Communications of the Association for Computing Machinery, 9:684

        double x = alpha, f = 0.0, z;

        if (x < 7) {
            f = 1;
            z = x - 1;
            while (++z < 7) {
                f *= z;
            }
            x = z;
            f = -Math.log(f);
        }
        z = 1 / (x * x);

        return
                f + (x - 0.5) * Math.log(x) - x + 0.918938533204673 +
                        (((-0.000595238095238 * z + 0.000793650793651) *
                                z - 0.002777777777778) * z + 0.083333333333333) / x;
    }
    
    /**
     * Returns the incomplete gamma ratio I(x,alpha) where x is the upper
     * limit of the integration and alpha is the shape parameter.
     *
     * @param x              upper limit of integration
     * @param alpha          shape parameter
     * @param ln_gamma_alpha the log gamma function for alpha
     * @return the incomplete gamma ratio
     */
    private static double incompleteGamma(double x, double alpha, double ln_gamma_alpha) {
        // (1) series expansion     if (alpha>x || x<=1)
        // (2) continued fraction   otherwise
        // RATNEST FORTRAN by
        // Bhattacharjee GP (1970) The incomplete gamma integral.  Applied Statistics,
        // 19: 285-287 (AS32)

        double accurate = 1e-8, overflow = 1e30;
        double factor, gin, rn, a, b, an, dif, term;
        double pn0, pn1, pn2, pn3, pn4, pn5;

        if (x == 0.0) {
            return 0.0;
        }
        if (x < 0.0 || alpha <= 0.0) {
            throw new IllegalArgumentException("Arguments out of bounds");
        }

        factor = Math.exp(alpha * Math.log(x) - x - ln_gamma_alpha);

        if (x > 1 && x >= alpha) {
            // continued fraction
            a = 1 - alpha;
            b = a + x + 1;
            term = 0;
            pn0 = 1;
            pn1 = x;
            pn2 = x + 1;
            pn3 = x * b;
            gin = pn2 / pn3;

            do {
                a++;
                b += 2;
                term++;
                an = a * term;
                pn4 = b * pn2 - an * pn0;
                pn5 = b * pn3 - an * pn1;

                if (pn5 != 0) {
                    rn = pn4 / pn5;
                    dif = Math.abs(gin - rn);
                    if (dif <= accurate) {
                        if (dif <= accurate * rn) {
                            break;
                        }
                    }

                    gin = rn;
                }
                pn0 = pn2;
                pn1 = pn3;
                pn2 = pn4;
                pn3 = pn5;
                if (Math.abs(pn4) >= overflow) {
                    pn0 /= overflow;
                    pn1 /= overflow;
                    pn2 /= overflow;
                    pn3 /= overflow;
                }
            } while (true);
            gin = 1 - factor * gin;
        } else {
            // series expansion
            gin = 1;
            term = 1;
            rn = alpha;
            do {
                rn++;
                term *= x / rn;
                gin += term;
            }
            while (term > accurate);
            gin *= factor / alpha;
        }
        return gin;
    }
	
	public static double binomialExactCDF(int n, int k, double p)
	{
		double sum = 0;
		for(int i = 0; i < k; i++)
		{
			sum+= binomial(n,k)*Math.pow(p,k)*Math.pow(1-p,n-k);
		}
		return sum;
	}
	
	public static double binomial(int n, int k)
	{
		return factorial(n)/(factorial(k)*factorial(n-k));
	}
	
	public static double normalCDF(double x, double mu, double sd)
	{
		return 0.5*(1+erf((x-mu)/(sd*Math.sqrt(2)) ) );
	}
	
	public static double erf(double x)
	{
		double[] a = {1.00002368,0.37409196,0.09678418,-0.18628806,0.27886807,-1.13520398,1.48851587,-0.82215223,0.17087277};
		double exp = -x*x-1.26551223;
		double t = 1/(1.0+0.5*Math.abs(x));
		for (int i = 0; i < a.length; i++)
		{
			exp+=a[i]*Math.pow(t,i+1);
		}
		if(x >= 0)
		return 1-t*Math.exp(exp);
		else
			return t*Math.exp(exp)-1;
	}
	
	public static double factorial(int i)
	{
		if(i < 2)
			return 1;
		else if(i <= 20)
		{
			long count = 1;
			for (int j = 2; j <= i; j++)
				count*=j;
			return count;
		}
		else
			return (Math.sqrt(2*Math.PI*i)*Math.pow(i/Math.E,i));
	}
}
