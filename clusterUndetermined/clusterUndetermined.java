package clusterUndetermined;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Scanner;
import java.util.zip.GZIPInputStream;

public class clusterUndetermined {

	private class wrapper
	{
		boolean paired;
		int n = 0;
		int bytesPerRead = 200;
		HashMap<String,Integer> counts = new HashMap<String,Integer>();
		Scanner s = null;
		int sampleSize = 0;
		public wrapper(Scanner s, int ss)
		{
			this.s = s;
			sampleSize =ss;
		}
	}
	
		private HashMap<String,String> knownTags = new HashMap<String,String>();
		private void setupKnownTags()
		{
			knownTags.put("TAGATCGC","Nextera N501");
			knownTags.put("CTCTCTAT","Nextera N502");
			knownTags.put("TATCCTCT","Nextera N503");
			knownTags.put("AGAGTAGA","Nextera N504");
			knownTags.put("GTAAGGAG","Nextera N505");
			knownTags.put("ACTGCATA","Nextera N506");
			knownTags.put("AAGGAGTA","Nextera N507");
			knownTags.put("CTAAGCCT","Nextera N508");
			knownTags.put("TCGCCTTA","Nextera N701");
			knownTags.put("CTAGTACG","Nextera N702");
			knownTags.put("TTCTGCCT","Nextera N703");
			knownTags.put("GCTCAGGA","Nextera N704");
			knownTags.put("AGGAGTCC","Nextera N705");
			knownTags.put("CATGCCTA","Nextera N706");
			knownTags.put("GTAGAGAG","Nextera N707");
			knownTags.put("CCTCTCTG","Nextera N708");
			knownTags.put("AGCGTAGC","Nextera N709");
			knownTags.put("CAGCCTCG","Nextera N710");
			knownTags.put("TGCCTCTT","Nextera N711");
			knownTags.put("TCCTCTAC","Nextera N712");
			knownTags.put("ATCACG","TrueSeq TSBC01");
			knownTags.put("CGATGT","TrueSeq TSBC02");
			knownTags.put("TTAGGC","TrueSeq TSBC03");
			knownTags.put("TGACCA","TrueSeq TSBC04");
			knownTags.put("ACAGTG","TrueSeq TSBC05");
			knownTags.put("GCCAAT","TrueSeq TSBC06");
			knownTags.put("CAGATC","TrueSeq TSBC07");
			knownTags.put("ACTTGA","TrueSeq TSBC08");
			knownTags.put("GATCAG","TrueSeq TSBC09");
			knownTags.put("TAGCTT","TrueSeq TSBC10");
			knownTags.put("GGCTAC","TrueSeq TSBC11");
			knownTags.put("CTTGTA","TrueSeq TSBC12");
			knownTags.put("AGTCAA","TrueSeq TSBC13");
			knownTags.put("AGTTCC","TrueSeq TSBC14");
			knownTags.put("ATGTCA","TrueSeq TSBC15");
			knownTags.put("CCGTCC","TrueSeq TBSC16");
			knownTags.put("GTAGAG","TrueSeq TBSC17");
			knownTags.put("GTCCGC","TrueSeq TBSC18");
			knownTags.put("GTGAAA","TrueSeq TBSC19");
			knownTags.put("GTGGCC","TrueSeq TBSC20");
			knownTags.put("GTTTCG","TrueSeq TBSC21");
			knownTags.put("CGTACG","TrueSeq TBSC22");
			knownTags.put("GAGTGG","TrueSeq TBSC23");
			knownTags.put("GGTAGC","TrueSeq TBSC24");
			knownTags.put("ACTGAT","TrueSeq TBSC25");
			knownTags.put("ATGAGC","TrueSeq TBSC26");
			knownTags.put("ATTCCT","TrueSeq TBSC27");
			knownTags.put("CAAAAG","TrueSeq TBSC28");
			knownTags.put("CAACTA","TrueSeq TBSC29");
			knownTags.put("CACCGG","TrueSeq TBSC30");
			knownTags.put("CACGAT","TrueSeq TBSC31");
			knownTags.put("CACTCA","TrueSeq TBSC32");
			knownTags.put("CAGGCG","TrueSeq TBSC33");
			knownTags.put("CATGGC","TrueSeq TBSC34");
			knownTags.put("CATTTT","TrueSeq TBSC35");
			knownTags.put("CCAACA","TrueSeq TBSC36");
			knownTags.put("CGGAAT","TrueSeq TBSC37");
			knownTags.put("CTAGCT","TrueSeq TBSC38");
			knownTags.put("CTATAC","TrueSeq TBSC39");
			knownTags.put("CTCAGA","TrueSeq TBSC40");
			knownTags.put("GACGAC","TrueSeq TBSC41");
			knownTags.put("GCGCTA","NextFlex NFBC41");
			knownTags.put("TAATCG","TrueSeq TBSC42");
			knownTags.put("TACAGC","TrueSeq TBSC43");
			knownTags.put("TATAAT","TrueSeq TBSC44");
			knownTags.put("TCATTC","TrueSeq TBSC45");
			knownTags.put("TCCCGA","TrueSeq TBSC46");			
			knownTags.put("TCGAAG","TrueSeq TBSC47");			
			knownTags.put("TCGGCA","TrueSeq TBSC48");
			knownTags.put("AACGTGAT","RNA HT Adapter 1");
			knownTags.put("AAACATCG","RNA HT Adapter 2");
			knownTags.put("ATGCCTAA","RNA HT Adapter 3");
			knownTags.put("AGTGGTCA","RNA HT Adapter 4");
			knownTags.put("ACCACTGT","RNA HT Adapter 5");
			knownTags.put("ACATTGGC","RNA HT Adapter 6");
			knownTags.put("CAGATCTG","RNA HT Adapter 7");
			knownTags.put("CATCAAGT","RNA HT Adapter 8");
			knownTags.put("CGCTGATC","RNA HT Adapter 9");
			knownTags.put("ACAAGCTA","RNA HT Adapter 10");
			knownTags.put("CTGTAGCC","RNA HT Adapter 11");
			knownTags.put("AGTACAAG","RNA HT Adapter 12");
			knownTags.put("AACAACCA","RNA HT Adapter 13");
			knownTags.put("AACCGAGA","RNA HT Adapter 14");
			knownTags.put("AACGCTTA","RNA HT Adapter 15");
			knownTags.put("AAGACGGA","RNA HT Adapter 16");
			knownTags.put("AAGGTACA","RNA HT Adapter 17");
			knownTags.put("ACACAGAA","RNA HT Adapter 18");
			knownTags.put("ACAGCAGA","RNA HT Adapter 19");
			knownTags.put("ACCTCCAA","RNA HT Adapter 20");
			knownTags.put("ACGCTCGA","RNA HT Adapter 21");
			knownTags.put("ACGTATCA","RNA HT Adapter 22");
			knownTags.put("ACTATGCA","RNA HT Adapter 23");
			knownTags.put("AGAGTCAA","RNA HT Adapter 24");
			knownTags.put("AGATCGCA","RNA HT Adapter 25");
			knownTags.put("AGCAGGAA","RNA HT Adapter 26");
			knownTags.put("AGTCACTA","RNA HT Adapter 27");
			knownTags.put("ATCCTGTA","RNA HT Adapter 28");
			knownTags.put("ATTGAGGA","RNA HT Adapter 29");
			knownTags.put("CAACCACA","RNA HT Adapter 30");
			knownTags.put("GACTAGTA","RNA HT Adapter 31");
			knownTags.put("CAATGGAA","RNA HT Adapter 32");
			knownTags.put("CACTTCGA","RNA HT Adapter 33");
			knownTags.put("CAGCGTTA","RNA HT Adapter 34");
			knownTags.put("CATACCAA","RNA HT Adapter 35");
			knownTags.put("CCAGTTCA","RNA HT Adapter 36");
			knownTags.put("CCGAAGTA","RNA HT Adapter 37");
			knownTags.put("CCGTGAGA","RNA HT Adapter 38");
			knownTags.put("CCTCCTGA","RNA HT Adapter 39");
			knownTags.put("CGAACTTA","RNA HT Adapter 40");
			knownTags.put("CGACTGGA","RNA HT Adapter 41");
			knownTags.put("CGCATACA","RNA HT Adapter 42");
			knownTags.put("CTCAATGA","RNA HT Adapter 43");
			knownTags.put("CTGAGCCA","RNA HT Adapter 44");
			knownTags.put("CTGGCATA","RNA HT Adapter 45");
			knownTags.put("GAATCTGA","RNA HT Adapter 46");
			knownTags.put("CAAGACTA","RNA HT Adapter 47");
			knownTags.put("GAGCTGAA","RNA HT Adapter 48");
			knownTags.put("GATAGACA","RNA HT Adapter 49");
			knownTags.put("GCCACATA","RNA HT Adapter 50");
			knownTags.put("GCGAGTAA","RNA HT Adapter 51");
			knownTags.put("GCTAACGA","RNA HT Adapter 52");
			knownTags.put("GCTCGGTA","RNA HT Adapter 53");
			knownTags.put("GGAGAACA","RNA HT Adapter 54");
			knownTags.put("GGTGCGAA","RNA HT Adapter 55");
			knownTags.put("GTACGCAA","RNA HT Adapter 56");
			knownTags.put("GTCGTAGA","RNA HT Adapter 57");
			knownTags.put("GTCTGTCA","RNA HT Adapter 58");
			knownTags.put("GTGTTCTA","RNA HT Adapter 59");
			knownTags.put("TAGGATGA","RNA HT Adapter 60");
			knownTags.put("TATCAGCA","RNA HT Adapter 61");
			knownTags.put("TCCGTCTA","RNA HT Adapter 62");
			knownTags.put("TCTTCACA","RNA HT Adapter 63");
			knownTags.put("TGAAGAGA","RNA HT Adapter 64");
			knownTags.put("TGGAACAA","RNA HT Adapter 65");
			knownTags.put("TGGCTTCA","RNA HT Adapter 66");
			knownTags.put("TGGTGGTA","RNA HT Adapter 67");
			knownTags.put("TTCACGCA","RNA HT Adapter 68");
			knownTags.put("AACTCACC","RNA HT Adapter 69");
			knownTags.put("AAGAGATC","RNA HT Adapter 70");
			knownTags.put("AAGGACAC","RNA HT Adapter 71");
			knownTags.put("AATCCGTC","RNA HT Adapter 72");
			knownTags.put("AATGTTGC","RNA HT Adapter 73");
			knownTags.put("ACACGACC","RNA HT Adapter 74");
			knownTags.put("ACAGATTC","RNA HT Adapter 75");
			knownTags.put("AGATGTAC","RNA HT Adapter 76");
			knownTags.put("AGCACCTC","RNA HT Adapter 77");
			knownTags.put("AGCCATGC","RNA HT Adapter 78");
			knownTags.put("AGGCTAAC","RNA HT Adapter 79");
			knownTags.put("ATAGCGAC","RNA HT Adapter 80");
			knownTags.put("ATCATTCC","RNA HT Adapter 81");
			knownTags.put("ATTGGCTC","RNA HT Adapter 82");
			knownTags.put("CAAGGAGC","RNA HT Adapter 83");
			knownTags.put("CACCTTAC","RNA HT Adapter 84");
			knownTags.put("CCATCCTC","RNA HT Adapter 85");
			knownTags.put("CCGACAAC","RNA HT Adapter 86");
			knownTags.put("CCTAATCC","RNA HT Adapter 87");
			knownTags.put("CCTCTATC","RNA HT Adapter 88");
			knownTags.put("CGACACAC","RNA HT Adapter 89");
			knownTags.put("CGGATTGC","RNA HT Adapter 90");
			knownTags.put("CTAAGGTC","RNA HT Adapter 91");
			knownTags.put("GAACAGGC","RNA HT Adapter 92");
			knownTags.put("GACAGTGC","RNA HT Adapter 93");
			knownTags.put("GAGTTAGC","RNA HT Adapter 94");
			knownTags.put("GATGAATC","RNA HT Adapter 95");
			knownTags.put("GCCAAGAC","RNA HT Adapter 96");
		}
	
		public clusterUndetermined(String filename, int sampleSize, boolean verbose) throws IOException
		{
			setupKnownTags();
			//the first arg is the filename! The rest are optional options
			InputStream fileStream = new FileInputStream(filename);
			boolean gzipped =false;
			if(filename.endsWith(".gz"));
			{
				fileStream = new GZIPInputStream(fileStream);
				gzipped = true;
			}
			Scanner s = new Scanner(fileStream);
			//System.out.println(Methods.binomialCDF(2000000,55,1.0/Math.pow(4,8)));
			wrapper w = getTagCounts(new wrapper(s,sampleSize));
			File f = new File(filename);
			long remaining_size = f.length();
			int bytes_per_read =w.bytesPerRead;
			int gzip_multiplier = 10;
			if(sampleSize > 5000000 ) gzip_multiplier = 9;
			if(!gzipped)
				gzip_multiplier = 1;
			long total_reads = sampleSize+gzip_multiplier*remaining_size/bytes_per_read;
			int percent_unrecognized = (int) ((100*total_reads)/(400000000));
			System.out.println("* There were about "+(total_reads/1000000)+"xE6 unrecognized reads or "+percent_unrecognized+"% of the ~4xe8 of the expected reads!  ");
			if(percent_unrecognized > 90)
				System.out.println("* Check that the right sample sheet was used during demultiplexing.  ");
			else if(percent_unrecognized < 5)
				System.out.println("* The vast majority of reads had proper barcodes. Errors were minimal.  ");
			else
				System.out.println("* There was a moderate fraction of unrecognized reads... potential contamination, wrong barcode(s), or technical errors:  ");
			if(w.paired)
				sampleSize*=2;
			//lets output the overrepresented barcodes
			System.out.println();
			System.out.println("---------------\t---------------\t------\t------\t------\t------\t---------------------");
			System.out.println("Barcode        \tReverse Comp.  \tCount\tEnrich\tExpect\tp-value\tDescription          ");
			System.out.println("---------------\t---------------\t------\t------\t------\t------\t---------------------");
			ArrayList<String> tags = new ArrayList<String>();
			ArrayList<Double> pvalues = new ArrayList<Double>();
			ArrayList<Integer> cs = new ArrayList<Integer>();
			ArrayList<Integer> expecteds = new ArrayList<Integer>();
			ArrayList<String> notes = new ArrayList<String>();
			int[] ncounts = new int[w.n+1];
			int totalBarcodesWithNs = 0;

			int totalSignificantBarcodes = 0;
			for (String b: w.counts.keySet())
			{
				int ns = 0;
				char[] chars= b.toCharArray();
				for(char c: chars)
				{
					if(c == 'N')
						ns++;
				}								
				double p = (1.0/Math.pow(4,w.n-ns))*Math.pow(0.25,Math.max(0,ns-1));  //assume uniform base probability and 25% chance of an extra N once you have one
				if(ns > 0) p*=0.001; 
				int expected = (int)Math.round(p*sampleSize);
				double pvalue = 1-Methods.binomialCDF(sampleSize, w.counts.get(b), p);
				if(pvalue < 0) pvalue = 0;
				if(Double.isNaN(pvalue))
					pvalue = 1;
				int i = 0;
				while(i < cs.size())
				{
					if(pvalue> pvalues.get(i))
						i++;
					else if(pvalue == pvalues.get(i))
					{
						if(cs.get(i) > w.counts.get(b))
							i++;
						else
							break;
					}
					else
						break;
				}
				if(verbose)
				{
					tags.add(i,b);
					pvalues.add(i,pvalue);
					cs.add(i,w.counts.get(b));
					expecteds.add(i,expected);
					if(knownTags.get(b)!= null)
						notes.add(i,knownTags.get(b));
					else if(knownTags.get(getReverseComplement(b)) != null)
					{
						notes.add(i,knownTags.get(getReverseComplement(b)));
					}
					else if(ns == 0)
						notes.add(i,"Unknown Barcode");
					else 
						notes.add(i,"N-containing");
					if(pvalue < 0.05)
						totalSignificantBarcodes+=w.counts.get(b);
				}
				ncounts[ns]+=w.counts.get(b);
				totalBarcodesWithNs+=w.counts.get(b);
			}
			if(verbose)
			{
				for(int i = 0; i < Math.min(10, tags.size()); i++)
				{
					double enrichment = ((double)cs.get(i))/expecteds.get(i);
					String enrichString = String.format("%3.1f",enrichment);
					if(enrichment > 100000000000.0) enrichString = "Inf";
					System.out.println(String.format("%15s\t%15s\t%6d\t%6s\t%6d\t%3.3f\t%s\n",tags.get(i),getReverseComplement(tags.get(i)),cs.get(i),enrichString,expecteds.get(i),pvalues.get(i),notes.get(i)));
				}
				System.out.println("---------------\t------\t------\t------\t------\t---------------------");
				System.out.println();
				System.out.println("-----\t-------\t-------");
				System.out.println("#Ns  \tCount  \tPercent");
				System.out.println("-----\t-------\t-------");
				if(totalBarcodesWithNs == 0) totalBarcodesWithNs = 1;
				System.out.println(String.format("%5d\t%7d\t%3.3f\n",0,ncounts[0],(100.0*ncounts[0])/totalBarcodesWithNs));
				System.out.println(String.format("%5d\t%7d\t%3.3f\n",1,ncounts[1],(100.0*ncounts[1])/totalBarcodesWithNs));
				int twoPlus = totalBarcodesWithNs-ncounts[0]-ncounts[1];
				System.out.println(String.format("%5s\t%7d\t%3.3f\n","2+",twoPlus,(100.0*twoPlus)/totalBarcodesWithNs));
				System.out.println("-----\t-------\t-------");
			}
			if(((100.0*ncounts[0])/totalBarcodesWithNs) <90)
			{
				System.out.println("More than 10 % of the unrecognized barcodes had 1 or more Ns: the sequencer struggled to identify nts!  ");
			}
			if(totalSignificantBarcodes > 0.5*sampleSize)
			{
				System.out.println("Enriched barcodes make up "+(totalSignificantBarcodes/(0.01*sampleSize))+"% of all unrecognized barcodes.\n\nThe top barcode(s) were:  ");
				for(int i = 0; i < tags.size(); i++)
				{
					if(cs.get(i) > 0.66*cs.get(0))
					{
						System.out.println(tags.get(i)+"\t"+notes.get(i));
						System.out.println();
					}
					else
						break;
				}
			}
			

		}
	
	public static void main(String[] args) {
		if(args.length < 1)
		{
			System.err.println("Reads in a fastq file or gzipped fastq file and detects possible problems with the read barcodes.");
			System.err.println("Usage: clusterUndetermined unrecognized.fastq [reads_to_sample]");
			System.exit(1);
		}
		try{
			if(args.length > 2)
			{
				new clusterUndetermined(args[0],Integer.parseInt(args[1]), Boolean.parseBoolean(args[2]));
			}
			else if(args.length == 2)
				new clusterUndetermined(args[0],Integer.parseInt(args[1]),true);
			else
				new clusterUndetermined(args[0], 1000000, true);
		}
		catch (IOException e)
		{
			System.err.println("IO error occured... check that the file: "+args[0]+" exists");
		}
		
	}
	
	private wrapper getTagCounts(wrapper w)
	{
		if(w.counts == null)
			w.counts= new HashMap<String,Integer>();
		if(w.s.hasNextLine())
		{
			String[] at = w.s.nextLine().split(":");
			String barcode = at[at.length-1];
			if(barcode.indexOf('+') != -1)
			{
				//dealing with paired reads
				String[] barcodes = barcode.split("\\+");
				w.n = barcodes[0].length();
				w.counts.put(barcodes[0],1);
				w.counts.put(barcodes[1], 1);
				w.paired = true;
			}
			else{
				w.n = barcode.length();
				w.counts.put(barcode,1);
			}

		}
		int lines = 1;
		boolean read_one = false;
		while(w.s.hasNextLine() && (lines++ < w.sampleSize*4 || w.sampleSize ==-1))
		{
			String line = w.s.nextLine();
			if(!read_one) w.bytesPerRead+=line.length();
			if(line.startsWith("@"))
			{
				read_one = true;
				String[] at = line.split(":");
				String barcode = at[at.length-1];
				if(w.paired)
				{
					String[] barcodes = barcode.split("\\+");
					if(w.counts.get(barcodes[0]) == null)
						w.counts.put(barcodes[0],1);
					else
						w.counts.put(barcodes[0],w.counts.get(barcodes[0])+1);
					if(w.counts.get(barcodes[1]) == null)
						w.counts.put(barcodes[1],1);
					else
						w.counts.put(barcodes[1],w.counts.get(barcodes[1])+1);
				}
				else
				{
					if(w.counts.get(barcode) == null)
						w.counts.put(barcode,1);
					else
						w.counts.put(barcode,w.counts.get(barcode)+1);
				}

			}
		}
		w.sampleSize=lines/4;
		return w;
	}
	private String getReverseComplement(String read)
	{
		String result = "";
		
		for(int i = read.length()-1; i >= 0; i--)
		{
			if(read.charAt(i)=='A')result+="T";
			else if(read.charAt(i)=='T') result+="A";
			else if(read.charAt(i)=='G') result+="C";
			else if(read.charAt(i)=='C') result+="G";
			else
				result+="N";
		}
		return result;
	}
}
